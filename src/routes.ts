import TempHomePage from "./TempHomePage";
import { authRoutes } from "./modules/auth/routes";
import { foodRoutes } from "./modules/food/routes";
import { historyRoutes } from "./modules/history/routes";
import { menuRoutes } from "./modules/menu/routes";

export const routes: {
  path: string;
  component: () => JSX.Element;
  noLayout: boolean;
  adminOnly?: boolean;
}[] = [
  ...authRoutes,
  ...menuRoutes,
  ...foodRoutes,
  ...historyRoutes,
  {
    path: "/",
    component: TempHomePage,
    noLayout: false,
    adminOnly: false,
  },
];
