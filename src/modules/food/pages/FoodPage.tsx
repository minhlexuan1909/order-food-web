import React, { Suspense } from "react";

const FoodWrapper = React.lazy(() => import("../components/FoodWrapper"));
const FoodPage = () => {
  return (
    <Suspense fallback={<></>}>
      <FoodWrapper />
    </Suspense>
  );
};

export default FoodPage;
