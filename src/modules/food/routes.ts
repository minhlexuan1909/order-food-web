import FoodPage from "./pages/FoodPage";

export const foodRoutes = [
  {
    path: "/food",
    component: FoodPage,
    noLayout: false,
    adminOnly: true,
  },
];
