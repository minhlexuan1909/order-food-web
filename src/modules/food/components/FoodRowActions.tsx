import "../assets/scss/foodRowActions.scss";

import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button } from "antd";
import store from "../../common/redux/store";
import { setFood } from "../redux/actions";
import { FoodType } from "../utils/types";

const FoodRowActions = (_: string, record: FoodType) => {
  const handleEditBtnClick = () => {
    store.dispatch(
      setFood({
        isShowModalActionFood: true,
        action: "EDIT",
        currentActionFood: record,
      })
    );
  };
  const handleDeleteBtnClick = () => {
    store.dispatch(
      setFood({
        isShowModalDeleteFood: true,
        currentActionFood: record,
      })
    );
  };

  return (
    <div className="food-row-actions">
      <Button onClick={handleEditBtnClick}>
        <EditOutlined />
      </Button>
      <Button type="primary" danger onClick={handleDeleteBtnClick}>
        <DeleteOutlined />
      </Button>
    </div>
  );
};

export default FoodRowActions;
