import { Button, Table } from "antd";
import { useQuery } from "../../base/utils";
import { FoodStoreType, GetFoodsQueryRequest } from "../utils/types";
import { useDispatch, useSelector } from "react-redux";
import { FOOD_COLUMNS } from "../utils/constants";
import { useEffect, useState } from "react";
import { getFoods, setFood } from "../redux/actions";

import "../assets/scss/foodWrapper.scss";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { PlusCircleOutlined } from "@ant-design/icons";
import ModalActionFood from "./ModalActionFood";
import ModalConfirmDelete from "./ModalConfirmDelete";
import TableFilters from "../../base/components/TableFilters";
import { Dayjs } from "dayjs";

const FoodWrapper = () => {
  const dispatch = useDispatch();
  const { query, handleQuery } = useQuery<GetFoodsQueryRequest>();
  const { page, name, date } = query;

  const { count, foods, state } = useSelector(
    (state: { food: FoodStoreType }) => state.food
  );

  const [queryCount, setQueryCount] = useState(0);

  const handleCreateFoodBtn = () => {
    dispatch(
      setFood({
        isShowModalActionFood: true,
        action: "CREATE",
        currentActionFood: null,
      })
    );
  };

  const handlePageChange = (page: number) => {
    handleQuery({ page });
  };

  const handleTableFilter = (name: string, date: Dayjs | null) => {
    handleQuery({
      page: 1,
      ...(name && { name }),
      ...(date && { date: date.format("DD-MM-YYYY") }),
    });
    setQueryCount((prev) => prev + 1);
  };

  useEffect(() => {
    if (!page) {
      handleQuery({ page: 1 });
    }
  }, []);

  useEffect(() => {
    dispatch(getFoods({ ...query, page: page || 1 }));
  }, [page, name, date, queryCount]);

  return (
    <div className="food-wrapper">
      <div className="add-food-btn-wrapper">
        <Button type="primary" onClick={handleCreateFoodBtn}>
          <PlusCircleOutlined /> Thêm món ăn
        </Button>
      </div>
      <TableFilters
        initialValues={{ name, date }}
        onFilter={handleTableFilter}
      />
      <Table
        loading={state === COMPONENT_STATES.LOADING}
        scroll={{ y: "calc(100vh - 350px)", x: "900px" }}
        columns={FOOD_COLUMNS}
        dataSource={foods}
        pagination={{
          total: count,
          current: Number(page) || 1,
          pageSize: 10,
          showSizeChanger: false,
          onChange: handlePageChange,
        }}
        rowKey={"id"}
      ></Table>
      <ModalActionFood />
      <ModalConfirmDelete />
    </div>
  );
};

export default FoodWrapper;
