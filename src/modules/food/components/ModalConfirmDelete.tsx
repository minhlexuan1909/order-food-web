import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { deleteFood, setFood } from "../redux/actions";
import { FoodStoreType } from "../utils/types";

const ModalConfirmDelete = () => {
  const dispatch = useDispatch();

  const { isShowModalDeleteFood, currentActionFood } = useSelector(
    (state: { food: FoodStoreType }) => state.food
  );

  const handleOkModal = () => {
    dispatch(deleteFood());
  };

  const handleModalCancel = () => {
    dispatch(setFood({ isShowModalDeleteFood: false }));
  };

  return (
    <Modal
      centered
      open={isShowModalDeleteFood}
      onOk={handleOkModal}
      onCancel={handleModalCancel}
      title={`Xóa món ăn ${currentActionFood?.name || ""}`}
      okText="Xác nhận"
      cancelText="Hủy"
    >
      Bạn có chắc chắn muốn xóa món ăn này?
    </Modal>
  );
};

export default ModalConfirmDelete;
