import { Button, Form, Input, InputNumber, Modal, UploadFile } from "antd";
import { useForm } from "antd/es/form/Form";
import { FoodFormData, FoodStoreType } from "../utils/types";
import TextArea from "antd/es/input/TextArea";
import { UploadOutlined } from "@ant-design/icons";
import { UploadRequestOption } from "rc-upload/lib/interface";
import { useState, useEffect } from "react";
import Upload, { RcFile } from "antd/es/upload";
import { useDispatch, useSelector } from "react-redux";
import { createFood, setFood, updateFood } from "../redux/actions";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { ALLOWED_IMAGE_TYPES } from "../utils/constants";
import { BASE_URL } from "../../base/utils/constants";

const ModalActionFood = () => {
  const dispatch = useDispatch();

  const { isShowModalActionFood, action, currentActionFood, state } =
    useSelector((state: { food: FoodStoreType }) => state.food);

  const [form] = useForm<FoodFormData>();

  const [fileList, setFileList] = useState<UploadFile[]>([]);

  useEffect(() => {
    setFileList(
      currentActionFood?.image
        ? [
            {
              name: currentActionFood.image.split("/").pop() || "",
              uid: "",
              url: currentActionFood.image,
            },
          ]
        : []
    );
  }, [currentActionFood?.image]);

  const handleModalOk = () => {
    form.submit();
  };

  const handleModalCancel = () => {
    dispatch(setFood({ isShowModalActionFood: false }));
  };

  const handleCustomRequest = (options: UploadRequestOption) => {
    const { onSuccess, file } = options;
    const rcFile = file as RcFile;
    setFileList([
      { name: rcFile?.name, uid: rcFile.uid, url: URL.createObjectURL(rcFile) },
    ]);
    if (onSuccess) onSuccess("");
  };

  const handleImageRemove = (file: UploadFile) => {
    const filteredFileList = fileList.filter((item) => item.uid !== file.uid);
    setFileList(filteredFileList);
  };

  const handleSubmitForm = (values: FoodFormData) => {
    const formData = new FormData();
    formData.append("is_active", "");
    formData.append("name", values.name);
    formData.append("description", values.description);
    formData.append("default_price", values.default_price + "");
    const image = values.image?.file?.originFileObj;
    if (
      image &&
      (!currentActionFood?.image || image.url !== currentActionFood?.image)
    )
      formData.append("image", image);
    if (action === "CREATE") {
      dispatch(createFood(formData));
    } else if (action === "EDIT") {
      dispatch(updateFood(formData));
    }
  };

  useEffect(() => {
    form.setFieldsValue({
      name: currentActionFood?.name,
      description: currentActionFood?.description,
      default_price: currentActionFood?.default_price,
      image: {
        file: {
          originFileObj: {
            url: `${BASE_URL}${currentActionFood?.image_server_path}`,
          },
        },
      },
    });
  }, [currentActionFood]);

  return (
    <Modal
      forceRender
      open={isShowModalActionFood}
      onOk={handleModalOk}
      centered
      destroyOnClose
      title={`${
        action === "CREATE"
          ? "Thêm món ăn"
          : `Sửa món ăn ${currentActionFood?.name || ""}`
      } `}
      okText={`${action === "CREATE" ? "Thêm" : "Sửa"} món ăn`}
      okButtonProps={{ disabled: state === COMPONENT_STATES.LOADING }}
      cancelText="Hủy"
      onCancel={handleModalCancel}
      className="modal-action-food"
    >
      <Form form={form} onFinish={handleSubmitForm}>
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Vui lòng nhập tên món ăn" }]}
        >
          <Input autoComplete="foodName" placeholder="Tên món ăn" />
        </Form.Item>
        <Form.Item
          name="description"
          rules={[
            { required: true, message: "Vui lòng nhập mô tả của món ăn" },
          ]}
        >
          <TextArea autoComplete="foodDescription" placeholder="Mô tả" />
        </Form.Item>
        <Form.Item
          name={`default_price`}
          rules={[{ required: true, message: "Vui lòng nhập giá" }]}
        >
          <InputNumber
            style={{ width: "250px" }}
            type="number"
            controls={false}
            placeholder="Nhập giá"
            autoComplete={undefined}
          />
        </Form.Item>
        <Form.Item name="image">
          <Upload
            fileList={fileList}
            customRequest={handleCustomRequest}
            multiple={false}
            listType="picture"
            accept={ALLOWED_IMAGE_TYPES.join(", ")}
            onRemove={handleImageRemove}
          >
            <Button icon={<UploadOutlined />}>Tải ảnh lên</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalActionFood;
