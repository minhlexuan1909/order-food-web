import { FoodType } from "../utils/types";
import "../assets/scss/foodImage.scss";
import { BASE_URL } from "../../base/utils/constants";

const FoodImage = (_: string, record: FoodType) => {
  return (
    <img
      className="food-image"
      src={
        record.image_server_path ? `${BASE_URL}${record.image_server_path}` : ""
      }
      loading="lazy"
    />
  );
};

export default FoodImage;
