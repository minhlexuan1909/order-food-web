import { Dispatch } from "redux";
import { IActionRequest } from "../../common";
import { FoodStoreType, GetFoodsQueryRequest } from "../utils/types";
import { setFood } from "./actions";
import {
  apiCreateFood,
  apiDeleteFood,
  apiGetFoods,
  apiUpdateFood,
} from "./services";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { toast } from "react-toastify";

export const handleGetFoods = async (
  dispatch: Dispatch,
  action: IActionRequest<GetFoodsQueryRequest>
) => {
  try {
    dispatch(setFood({ foods: [], state: COMPONENT_STATES.LOADING }));
    const response = await apiGetFoods(action.params || {});
    dispatch(
      setFood({
        foods: response.data.results,
        state: COMPONENT_STATES.SUCCESS,
      })
    );
  } catch (err: any) {
    dispatch(
      setFood({
        state: COMPONENT_STATES.ERROR,
      })
    );
  }
};

export const handleCreateFood = async (
  dispatch: Dispatch,
  action: IActionRequest<FormData>,
  getState: () => { food: FoodStoreType }
) => {
  try {
    if (action.params) {
      dispatch(setFood({ state: COMPONENT_STATES.LOADING }));
      const response = await apiCreateFood(action.params);
      dispatch(
        setFood({
          foods: [response.data, ...getState().food.foods],
          isShowModalActionFood: false,
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Tạo món ăn thành công");
    }
  } catch (err: any) {
    dispatch(
      setFood({
        state: COMPONENT_STATES.ERROR,
      })
    );
    const statusCode = err.response?.status;
    console.log("err", err.response);
    if (statusCode === 403) {
      toast.error("Bạn không có quyền tạo món ăn");
    }
  }
};

export const handleUpdateFood = async (
  dispatch: Dispatch,
  action: IActionRequest<FormData>,
  getState: () => { food: FoodStoreType }
) => {
  const id = getState().food.currentActionFood?.id;
  try {
    if (action.params && id) {
      dispatch(setFood({ state: COMPONENT_STATES.LOADING }));
      const response = await apiUpdateFood(id, action.params);
      const foods = getState().food.foods.map((food) => {
        if (food.id === id) {
          return response.data;
        }
        return food;
      });
      dispatch(
        setFood({
          foods,
          isShowModalActionFood: false,
          action: undefined,
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Cập nhật món ăn thành công");
    }
  } catch (err: any) {
    dispatch(
      setFood({
        state: COMPONENT_STATES.ERROR,
      })
    );
    const statusCode = err.response?.status;
    console.log("err", err.response);
    if (statusCode === 403) {
      toast.error("Bạn không có quyền cập nhật món ăn");
    }
  }
};

export const handleDeleteFood = async (
  dispatch: Dispatch,
  getState: () => { food: FoodStoreType }
) => {
  try {
    const id = getState().food.currentActionFood?.id;
    if (id) {
      dispatch(setFood({ state: COMPONENT_STATES.LOADING }));
      await apiDeleteFood(id);
      const foods = getState().food.foods.filter((food) => food.id !== id);
      dispatch(
        setFood({
          foods,
          isShowModalDeleteFood: false,
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Xóa món ăn thành công");
    }
  } catch (err: any) {
    dispatch(
      setFood({
        state: COMPONENT_STATES.ERROR,
      })
    );
    const statusCode = err.response?.status;
    console.log("err", err.response);
    if (statusCode === 403) {
      toast.error("Bạn không có quyền xóa món ăn");
    }
  }
};
