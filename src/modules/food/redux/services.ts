import { IPagination, IResponse } from "../../common";
import { Api } from "../../common/lib/httpRequest";
import { FoodType, GetFoodsQueryRequest } from "../utils/types";

export const apiGetFoods = (
  params: GetFoodsQueryRequest
): Promise<IResponse<IPagination<FoodType>>> => {
  return Api.get("/api/foods/", params);
};

export const apiCreateFood = (
  params: FormData
): Promise<IResponse<FoodType>> => {
  return Api.post("/api/foods/", params);
};

export const apiUpdateFood = (
  id: number,
  params: FormData
): Promise<IResponse<FoodType>> => {
  return Api.patch(`/api/foods/${id}/`, params);
};

export const apiDeleteFood = (id: number): Promise<any> => {
  return Api.delete(`/api/foods/${id}/`, {});
};
