export const ACTION_TYPES = {
  GET_FOOD: Symbol("GET_FOOD"),
  SET_FOOD: Symbol("SET_FOOD"),
  CREATE_FOOD: Symbol("CREATE_FOOD"),
  UPDATE_FOOD: Symbol("UPDATE_FOOD"),
  DELETE_FOOD: Symbol("DELETE_FOOD"),
};
