import { IAction } from "../../common";
import ReducerRegistry from "../../common/redux/ReducerRegistry";
import { FoodStoreType } from "../utils/types";
import { ACTION_TYPES } from "./actionTypes";

const initState: FoodStoreType = {
  isShowModalActionFood: false,
  isShowModalDeleteFood: false,
  action: "CREATE",
  state: null,
  currentActionFood: null,
  foods: [],
  count: 0,
};

const reducer = (state = initState, action: IAction<any>) => {
  switch (action.type) {
    case ACTION_TYPES.SET_FOOD: {
      return {
        ...state,
        ...action.response,
      };
    }
    default:
      return state;
  }
};
ReducerRegistry.register("food", reducer);
