import { IAction, IActionRequest } from "../../common";
import { FoodStoreType, GetFoodsQueryRequest } from "../utils/types";
import { ACTION_TYPES } from "./actionTypes";

export const setFood = (
  response: Partial<FoodStoreType>
): IAction<Partial<FoodStoreType>> => {
  return {
    type: ACTION_TYPES.SET_FOOD,
    response,
  };
};

export const getFoods = (
  params?: GetFoodsQueryRequest
): IActionRequest<GetFoodsQueryRequest> => {
  return {
    type: ACTION_TYPES.GET_FOOD,
    params,
  };
};

export const createFood = (params: FormData): IActionRequest<FormData> => {
  return {
    type: ACTION_TYPES.CREATE_FOOD,
    params,
  };
};

export const updateFood = (params: FormData): IActionRequest<FormData> => {
  return {
    type: ACTION_TYPES.UPDATE_FOOD,
    params,
  };
};

export const deleteFood = (): IActionRequest<undefined> => {
  return {
    type: ACTION_TYPES.DELETE_FOOD,
  };
};
