import { Store } from "redux";

import { IActionRequest } from "../../common";
import MiddlewareRegistry from "../../common/redux/MiddlewareRegistry";
import { ACTION_TYPES } from "./actionTypes";
import {
  handleCreateFood,
  handleDeleteFood,
  handleGetFoods,
  handleUpdateFood,
} from "./functions";

export const middleware =
  ({ dispatch, getState }: Store) =>
  (next: Function) =>
  async (action: IActionRequest<any>) => {
    next(action);
    switch (action.type) {
      case ACTION_TYPES.GET_FOOD: {
        return await handleGetFoods(dispatch, action);
      }
      case ACTION_TYPES.CREATE_FOOD: {
        return await handleCreateFood(dispatch, action, getState);
      }
      case ACTION_TYPES.UPDATE_FOOD: {
        return await handleUpdateFood(dispatch, action, getState);
      }
      case ACTION_TYPES.DELETE_FOOD: {
        return await handleDeleteFood(dispatch, getState);
      }
    }
  };

MiddlewareRegistry.register(middleware);
