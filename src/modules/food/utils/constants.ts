import TableCellCreatedDate from "../../base/components/TableCellCreatedDate";
import FoodImage from "../components/FoodImage";
import FoodRowActions from "../components/FoodRowActions";
import { FoodType } from "./types";

export const FOOD_COLUMNS = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
    width: 50,
  },
  {
    title: "Tên món ăn",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Ảnh",
    dataIndex: "id",
    key: "id",
    render: FoodImage,
  },
  {
    title: "Mô tả",
    dataIndex: "description",
    key: "description",
  },
  {
    title: "Giá",
    dataIndex: "default_price",
    key: "default_price",
  },
  {
    title: "Ngày tạo",
    dataIndex: "created_at",
    key: "created_at",
    render: (_: string, record: FoodType) =>
      TableCellCreatedDate(record.created_at),
    width: 110,
  },
  {
    title: "Hành động",
    render: FoodRowActions,
    width: 150,
  },
];

export const ALLOWED_IMAGE_TYPES = ["image/jpeg", "image/jpg", "image/png"];
