import { ComponentStateType } from "../../common";

export interface FoodStoreType {
  isShowModalActionFood: boolean;
  isShowModalDeleteFood: boolean;
  action: "CREATE" | "EDIT";
  state: null | ComponentStateType;
  currentActionFood: FoodType | null;
  foods: FoodType[];
  count: number;
}

export interface FoodFormData {
  name: string;
  image: any;
  default_price: number;
  description: string;
}

export interface FoodType {
  id: number;
  name: string;
  image: string | null;
  image_server_path: string | null;
  description: string;
  default_price: number;
  created_at: string;
  updated_at: string;
  is_active: boolean;
}

export interface GetFoodsQueryRequest {
  [key: string]: any;
  name?: string;
  page?: number;
}
