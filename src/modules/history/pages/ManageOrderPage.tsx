import React, { Suspense } from "react";

const ManageOrderWrapper = React.lazy(
  () => import("../components/ManageOrderWrapper")
);

const ManageOrderPage = () => {
  return (
    <Suspense fallback={<></>}>
      <ManageOrderWrapper />
    </Suspense>
  );
};

export default ManageOrderPage;
