import React, { Suspense } from "react";

const HistoryWrapper = React.lazy(() => import("../components/HistoryWrapper"));

const HistoryPage = () => {
  return (
    <Suspense fallback={<></>}>
      <HistoryWrapper />
    </Suspense>
  );
};

export default HistoryPage;
