import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { Bill } from "../utils/types";
import store from "../../common/redux/store";
import { modifyBill } from "../redux/actions";

const TableManageOrderActionCell = (_: string, record: Bill) => {
  const handleConfirmPaymentBtnClick = () => {
    store.dispatch(modifyBill({ id: record.id, data: { is_paid: true } }));
  };

  const handleUnconfirmPaymentBtnClick = () => {
    store.dispatch(modifyBill({ id: record.id, data: { is_paid: false } }));
  };

  return (
    <>
      <Button
        type="primary"
        style={{
          background: "#28b44f",
          borderColor: "#28b44f",
          marginRight: "10px",
        }}
        onClick={handleConfirmPaymentBtnClick}
      >
        <CheckOutlined />
      </Button>
      <Button type="primary" danger onClick={handleUnconfirmPaymentBtnClick}>
        <CloseOutlined />
      </Button>
    </>
  );
};

export default TableManageOrderActionCell;
