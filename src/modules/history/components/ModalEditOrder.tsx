import { Form, InputNumber, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { HistoryStoreType } from "../utils/types";
import { modifyBill, setBills } from "../redux/actions";
import { useForm } from "antd/es/form/Form";

const ModalEditOrder = () => {
  const dispatch = useDispatch();
  const { isShowEditModal, currentActionBill } = useSelector(
    (state: { history: HistoryStoreType }) => state.history.bills
  );

  const [form] = useForm();

  const handleFinishForm = (values: any) => {
    if (currentActionBill) {
      dispatch(
        modifyBill({
          id: currentActionBill.id,
          data: {
            quantity: values.quantity,
          },
        })
      );
    }
  };

  const handleOkModal = () => {
    form.submit();
  };

  const handleCancelModal = () => {
    dispatch(setBills({ isShowEditModal: false }));
  };

  return (
    <Modal
      title={`Sửa số lượng đơn đặt cho menu ${currentActionBill?.menu.name}`}
      open={isShowEditModal}
      centered
      okText="Xác nhận"
      cancelText="Hủy"
      onOk={handleOkModal}
      onCancel={handleCancelModal}
    >
      <Form form={form} onFinish={handleFinishForm}>
        <Form.Item
          name="quantity"
          style={{ marginTop: 20 }}
          rules={[{ required: true, message: "Vui lòng nhập số lượng" }]}
        >
          <InputNumber
            style={{ width: "150px" }}
            type="number"
            step={1}
            min={1}
            placeholder="Nhập số lượng"
            autoComplete={undefined}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalEditOrder;
