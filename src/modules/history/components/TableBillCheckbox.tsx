import { Bill } from "../utils/types";
import { Checkbox } from "antd";

const TableBillCheckbox = (_: string, record: Bill) => {
  return <Checkbox checked={record.is_paid} />;
};

export default TableBillCheckbox;
