import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { HistoryStoreType } from "../utils/types";
import { deleteBill, setBills } from "../redux/actions";

const ModalDeleteOrder = () => {
  const dispatch = useDispatch();
  const { isShowDeleteModal, currentActionBill } = useSelector(
    (state: { history: HistoryStoreType }) => state.history.bills
  );

  const handleOkModal = () => {
    if (currentActionBill) {
      dispatch(deleteBill(currentActionBill.id));
    }
  };

  const handleCancelModal = () => {
    dispatch(setBills({ isShowDeleteModal: false }));
  };

  return (
    <Modal
      open={isShowDeleteModal}
      centered
      title="Bạn muốn hủy đơn đặt?"
      okText="Xác nhận"
      cancelText="Hủy"
      onOk={handleOkModal}
      onCancel={handleCancelModal}
    />
  );
};

export default ModalDeleteOrder;
