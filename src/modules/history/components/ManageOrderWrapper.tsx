import { useDispatch, useSelector } from "react-redux";
import { GetBillsQueryRequest, HistoryStoreType } from "../utils/types";
import { Table } from "antd";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { useQuery } from "../../base";
import { useEffect, useState } from "react";
import { getBillsAdmin } from "../redux/actions";

import "../assets/scss/manageOrderWrapper.scss";
import { MANAGE_ORDER } from "../utils/constants";
import TableFilters from "../../base/components/TableFilters";
import { Dayjs } from "dayjs";

const ManageOrderWrapper = () => {
  const dispatch = useDispatch();
  const { query, handleQuery } = useQuery<GetBillsQueryRequest>();
  const { page, name, date } = query;

  const { bills } = useSelector(
    (state: { history: HistoryStoreType }) => state.history
  );

  const [queryCount, setQueryCount] = useState(0);

  const handlePageChange = (page: number) => {
    handleQuery({ page });
  };

  const handleTableFilter = (name: string, date: Dayjs | null) => {
    handleQuery({
      page: 1,
      ...(name && { name }),
      ...(date && { date: date.format("DD-MM-YYYY") }),
    });
    setQueryCount((prev) => prev + 1);
  };

  useEffect(() => {
    if (!page) {
      handleQuery({ page: 1 });
    }
  }, []);

  useEffect(() => {
    dispatch(getBillsAdmin({ ...query, page: page || 1 }));
  }, [page, name, date, queryCount]);

  return (
    <div className="manage-order-wrapper">
      <TableFilters
        isShowNameFilter={false}
        initialValues={{ name, date }}
        onFilter={handleTableFilter}
      />
      <Table
        loading={bills.state === COMPONENT_STATES.LOADING}
        columns={MANAGE_ORDER}
        dataSource={bills.data}
        scroll={{ y: "calc(100vh - 200px)", x: "900px" }}
        rowKey={"id"}
        pagination={{
          total: bills.total,
          current: Number(page) || 1,
          pageSize: 10,
          showSizeChanger: false,
          onChange: handlePageChange,
        }}
      />
    </div>
  );
};

export default ManageOrderWrapper;
