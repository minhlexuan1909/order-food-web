import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Tooltip } from "antd";
import { Bill } from "../utils/types";
import moment from "moment";
import store from "../../common/redux/store";
import { setBills } from "../redux/actions";

const TableBillAction = (_: string, record: Bill) => {
  const currentHour = Number(moment().utc().utcOffset("+07:00").format("H"));
  const isCanOrder = currentHour >= 8 && currentHour <= 10;
  const isCanEdit =
    moment(record.created_at).format("YYYY-MM-DD") ===
      moment().format("YYYY-MM-DD") && isCanOrder;

  const handleEditBtnClick = () => {
    store.dispatch(
      setBills({ isShowEditModal: true, currentActionBill: record })
    );
  };
  const handleDeleteBtnClick = () => {
    store.dispatch(
      setBills({ isShowDeleteModal: true, currentActionBill: record })
    );
  };

  return (
    <>
      <Tooltip
        title="Quá thời gian chỉnh sửa"
        open={isCanEdit ? false : undefined}
      >
        <Button
          disabled={!isCanEdit}
          style={{ marginRight: "10px" }}
          onClick={handleEditBtnClick}
        >
          <EditOutlined />
        </Button>
      </Tooltip>
      <Tooltip
        title="Quá thời gian chỉnh sửa"
        open={isCanEdit ? false : undefined}
      >
        <Button
          disabled={!isCanEdit}
          type="primary"
          danger
          onClick={handleDeleteBtnClick}
        >
          <DeleteOutlined />
        </Button>
      </Tooltip>
    </>
  );
};

export default TableBillAction;
