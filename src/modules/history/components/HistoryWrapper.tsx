import { Table } from "antd";
import "../assets/scss/historyWrapper.scss";
import { useDispatch, useSelector } from "react-redux";
import { GetBillsQueryRequest, HistoryStoreType } from "../utils/types";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { ORDER_BILL_HISTORY } from "../utils/constants";
import { useEffect, useState } from "react";
import { getBills } from "../redux/actions";
import { useQuery } from "../../base";
import ModalDeleteOrder from "./ModalDeleteOrder";
import ModalEditOrder from "./ModalEditOrder";
import TableFilters from "../../base/components/TableFilters";
import { Dayjs } from "dayjs";

const HistoryWrapper = () => {
  const dispatch = useDispatch();
  const { query, handleQuery } = useQuery<GetBillsQueryRequest>();
  const { page, name, date } = query;

  const { bills } = useSelector(
    (state: { history: HistoryStoreType }) => state.history
  );

  const [queryCount, setQueryCount] = useState(0);

  const handlePageChange = (page: number) => {
    handleQuery({ page });
  };

  const handleTableFilter = (name: string, date: Dayjs | null) => {
    handleQuery({
      page: 1,
      ...(name && { name }),
      ...(date && { date: date.format("DD-MM-YYYY") }),
    });
    setQueryCount((prev) => prev + 1);
  };

  useEffect(() => {
    if (!page) {
      handleQuery({ page: 1 });
    }
  }, []);

  useEffect(() => {
    dispatch(getBills({ ...query, page: page || 1 }));
  }, [page, name, date, queryCount]);

  return (
    <div className="history-wrapper">
      <TableFilters
        isShowNameFilter={false}
        initialValues={{ name, date }}
        onFilter={handleTableFilter}
      />
      <Table
        loading={bills.state === COMPONENT_STATES.LOADING}
        columns={ORDER_BILL_HISTORY}
        dataSource={bills.data}
        scroll={{ y: "calc(100vh - 200px)", x: "900px" }}
        rowKey={"id"}
        pagination={{
          total: bills.total,
          current: Number(page) || 1,
          pageSize: 10,
          showSizeChanger: false,
          onChange: handlePageChange,
        }}
      />
      <ModalDeleteOrder />
      <ModalEditOrder />
    </div>
  );
};

export default HistoryWrapper;
