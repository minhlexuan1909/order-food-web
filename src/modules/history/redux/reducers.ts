import { IAction } from "../../common";
import ReducerRegistry from "../../common/redux/ReducerRegistry";
import { HistoryStoreType } from "../utils/types";
import { ACTION_TYPES } from "./actionTypes";

const initState: HistoryStoreType = {
  bills: {
    total: 0,
    data: [],
    currentActionBill: null,
    isShowEditModal: false,
    isShowDeleteModal: false,
    state: null,
  },
};

const reducer = (state = initState, action: IAction<any>) => {
  switch (action.type) {
    case ACTION_TYPES.SET_BIILS: {
      return {
        ...state,
        bills: {
          ...state.bills,
          ...action.response,
        },
      };
    }
    default:
      return state;
  }
};
ReducerRegistry.register("history", reducer);
