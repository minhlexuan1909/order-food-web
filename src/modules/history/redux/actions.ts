import { IAction, IActionRequest } from "../../common";
import {
  BillReducer,
  GetBillsQueryRequest,
  ModifyBillRequest,
} from "../utils/types";
import { ACTION_TYPES } from "./actionTypes";

export const getBills = (
  params?: GetBillsQueryRequest
): IActionRequest<GetBillsQueryRequest> => {
  return {
    type: ACTION_TYPES.GET_BILLS,
    params,
  };
};

export const getBillsAdmin = (
  params?: GetBillsQueryRequest
): IActionRequest<GetBillsQueryRequest> => {
  return {
    type: ACTION_TYPES.GET_BILLS_ADMIN,
    params,
  };
};

export const setBills = (
  response: Partial<BillReducer>
): IAction<Partial<BillReducer>> => {
  return {
    type: ACTION_TYPES.SET_BIILS,
    response,
  };
};

export const modifyBill = (params: {
  id: number;
  data: Partial<ModifyBillRequest>;
}): IActionRequest<{
  id: number;
  data: Partial<ModifyBillRequest>;
}> => {
  return {
    type: ACTION_TYPES.MODIFY_BILL,
    params,
  };
};

export const deleteBill = (id: number): IActionRequest<number> => {
  return {
    type: ACTION_TYPES.DELETE_BILL,
    params: id,
  };
};
