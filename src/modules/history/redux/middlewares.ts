import { Store } from "redux";

import { IActionRequest } from "../../common";
import MiddlewareRegistry from "../../common/redux/MiddlewareRegistry";
import { ACTION_TYPES } from "./actionTypes";
import {
  handleDeleteBill,
  handleGetBills,
  handleGetBillsAdmin,
  handleModifyBill,
} from "./functions";

export const middleware =
  ({ dispatch, getState }: Store) =>
  (next: Function) =>
  async (action: IActionRequest<any>) => {
    next(action);
    switch (action.type) {
      case ACTION_TYPES.GET_BILLS: {
        return await handleGetBills(dispatch, action);
      }
      case ACTION_TYPES.GET_BILLS_ADMIN: {
        return await handleGetBillsAdmin(dispatch, action);
      }
      case ACTION_TYPES.MODIFY_BILL: {
        return await handleModifyBill(dispatch, action, getState);
      }
      case ACTION_TYPES.DELETE_BILL: {
        return await handleDeleteBill(dispatch, action, getState);
      }
    }
  };

MiddlewareRegistry.register(middleware);
