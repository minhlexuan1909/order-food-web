import { IPagination, IResponse } from "../../common";
import { Api } from "../../common/lib/httpRequest";
import { Bill, GetBillsQueryRequest } from "../utils/types";

export const apiGetBills = (
  params: GetBillsQueryRequest | {}
): Promise<IResponse<IPagination<Bill>>> => {
  return Api.get("/api/bills/", params);
};

export const apiGetBillsAdmin = (
  params: GetBillsQueryRequest | {}
): Promise<IResponse<IPagination<Bill>>> => {
  return Api.get("/api/bills/admin/", params);
};

export const apiModifyBill = (
  id: number,
  data: any
): Promise<IResponse<Bill>> => {
  return Api.patch(`/api/bills/${id}/`, data);
};

export const apiDeleteBill = (id: number): Promise<undefined> => {
  return Api.delete(`/api/bills/${id}/`, {});
};
