import { Dispatch } from "redux";
import { IActionRequest } from "../../common";
import {
  GetBillsQueryRequest,
  HistoryStoreType,
  ModifyBillRequest,
} from "../utils/types";
import {
  apiDeleteBill,
  apiGetBills,
  apiGetBillsAdmin,
  apiModifyBill,
} from "./services";
import { setBills } from "./actions";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { toast } from "react-toastify";

export const handleGetBills = async (
  dispatch: Dispatch,
  action: IActionRequest<GetBillsQueryRequest>
) => {
  try {
    dispatch(setBills({ data: [], state: COMPONENT_STATES.LOADING }));
    const res = await apiGetBills(action.params || {});
    dispatch(
      setBills({
        total: res.data.count,
        data: res.data.results,
        state: COMPONENT_STATES.SUCCESS,
      })
    );
  } catch (err: any) {
    dispatch(setBills({ state: COMPONENT_STATES.ERROR }));
  }
};

export const handleGetBillsAdmin = async (
  dispatch: Dispatch,
  action: IActionRequest<GetBillsQueryRequest>
) => {
  try {
    dispatch(setBills({ data: [], state: COMPONENT_STATES.LOADING }));
    const res = await apiGetBillsAdmin(action.params || {});
    dispatch(
      setBills({
        total: res.data.count,
        data: res.data.results,
        state: COMPONENT_STATES.SUCCESS,
      })
    );
  } catch (err: any) {
    dispatch(setBills({ state: COMPONENT_STATES.ERROR }));
  }
};

export const handleModifyBill = async (
  dispatch: Dispatch,
  action: IActionRequest<{ id: number; data: Partial<ModifyBillRequest> }>,
  getState: () => { history: HistoryStoreType }
) => {
  try {
    if (action.params) {
      dispatch(setBills({ state: COMPONENT_STATES.LOADING }));
      const res = await apiModifyBill(action.params.id, action.params.data);
      dispatch(
        setBills({
          isShowEditModal: false,
          data: getState().history.bills.data.map((bill) => {
            if (bill.id === res.data.id) {
              return res.data;
            }
            return bill;
          }),
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Cập nhật thành công");
    }
  } catch (err: any) {
    toast.error("Cập nhật thất bại");
    dispatch(setBills({ state: COMPONENT_STATES.ERROR }));
  }
};

export const handleDeleteBill = async (
  dispatch: Dispatch,
  action: IActionRequest<number>,
  getState: () => { history: HistoryStoreType }
) => {
  try {
    if (action.params) {
      dispatch(setBills({ state: COMPONENT_STATES.LOADING }));
      await apiDeleteBill(action.params);
      dispatch(
        setBills({
          isShowDeleteModal: false,
          data: getState().history.bills.data.filter(
            (bill) => bill.id !== action.params
          ),
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Hủy đơn thành công");
    }
  } catch (err: any) {
    toast.error("Hủy đơn thất bại");
    dispatch(setBills({ state: COMPONENT_STATES.ERROR }));
  }
};
