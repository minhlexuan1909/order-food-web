import { ComponentStateType } from "../../common";

export interface HistoryStoreType {
  bills: BillReducer;
}

export interface BillReducer {
  total: number;
  data: Bill[];
  currentActionBill: Bill | null;
  isShowEditModal: boolean;
  isShowDeleteModal: boolean;
  state: null | ComponentStateType;
}

export interface User {
  phone: string;
  name: string;
  is_staff: true;
  is_superuser: true;
}

export interface Food {
  id: number;
  name: string;
  image: string;
  description: string;
  default_price: number;
  created_at: string;
  updated_at: string;
  is_active: boolean;
}

export interface Menu {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  foods: Food[];
}

export interface Bill {
  id: number;
  quantity: number;
  is_paid: boolean;
  created_at: string;
  updated_at: string;
  total_price: number;
  user: User;
  menu: Menu;
}

export interface GetBillsQueryRequest {
  [key: string]: any;
  page?: number;
}

export interface ModifyBillRequest {
  quantity: number;
  is_paid: boolean;
}
