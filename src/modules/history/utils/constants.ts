import TableCellCreatedDate from "../../base/components/TableCellCreatedDate";
import TableBillAction from "../components/TableBillAction";
import TableBillCheckbox from "../components/TableBillCheckbox";
import TableManageOrderActionCell from "../components/TableManageOrderActionCell";
import { Bill } from "./types";

export const ORDER_BILL_HISTORY = [
  {
    title: "Tên thực đơn",
    dataIndex: ["menu", "name"],
  },
  {
    title: "Số lượng",
    dataIndex: "quantity",
  },
  {
    title: "Tổng giá",
    dataIndex: "total_price",
  },
  {
    title: "Đã thanh toán",
    dataIndex: "is_paid",
    render: TableBillCheckbox,
  },
  {
    title: "Ngày đặt",
    render: (_: string, record: Bill) =>
      TableCellCreatedDate(record.created_at),
  },
  {
    title: "",
    render: TableBillAction,
  },
];

export const MANAGE_ORDER = [
  {
    title: "Người đặt",
    dataIndex: ["user", "name"],
  },
  {
    title: "Tên thực đơn",
    dataIndex: ["menu", "name"],
  },
  {
    title: "Số lượng",
    dataIndex: "quantity",
  },
  {
    title: "Tổng giá",
    dataIndex: "total_price",
  },
  {
    title: "Ngày đặt",
    render: (_: string, record: Bill) =>
      TableCellCreatedDate(record.created_at),
  },
  {
    title: "Đã thanh toán",
    dataIndex: "is_paid",
    render: TableBillCheckbox,
  },
  {
    title: "Xác nhận thanh toán",
    render: TableManageOrderActionCell,
  },
];
