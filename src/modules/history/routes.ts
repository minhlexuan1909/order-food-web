import HistoryPage from "./pages/HistoryPage";
import ManageOrderPage from "./pages/ManageOrderPage";

export const historyRoutes = [
  {
    path: "/history",
    component: HistoryPage,
    noLayout: false,
  },
  {
    path: "/manage-order",
    component: ManageOrderPage,
    noLayout: false,
    adminOnly: true,
  },
];
