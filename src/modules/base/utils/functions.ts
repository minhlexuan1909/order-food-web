export const parseQueryStringToObject = (queryString: string) => {
  const urlSearchParam = new URLSearchParams(queryString);
  const query: any = {};
  for (const [key, value] of urlSearchParam.entries()) {
    query[key] = value;
  }
  return query;
};

export const handleQuery = <T>(queryObj: T) => {
  const searchParams = new URLSearchParams(queryObj);
  history.push(`${location.pathname}?${searchParams.toString()}`);
};
