import { useHistory, useLocation } from "react-router-dom";
import { parseQueryStringToObject } from "./functions";

const useQuery = <
  T extends
    | string
    | string[][]
    | Record<string, string>
    | URLSearchParams
    | undefined
>() => {
  const history = useHistory();
  const location = useLocation();
  const query: T = parseQueryStringToObject(location.search);

  const handleQuery = (queryObj: T) => {
    const searchParams = new URLSearchParams(queryObj);
    history.push(`${location.pathname}?${searchParams.toString()}`);
  };

  return {
    query,
    handleQuery,
  };
};

export default useQuery;
