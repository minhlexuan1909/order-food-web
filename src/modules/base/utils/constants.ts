import {
  AppleOutlined,
  ContainerOutlined,
  DatabaseOutlined,
  MenuOutlined,
  UserAddOutlined,
} from "@ant-design/icons";

export const TAB_INFO = [
  {
    name: "Thực đơn",
    icon: MenuOutlined,
    linkTo: "/menu",
  },
  {
    name: "Món ăn",
    icon: AppleOutlined,
    linkTo: "/food",
    admin: true,
  },
  {
    name: "Lịch sử",
    icon: ContainerOutlined,
    linkTo: "/history",
  },
  {
    name: "Quản lý đặt hàng",
    icon: DatabaseOutlined,
    linkTo: "/manage-order",
    admin: true,
  },
  {
    name: "Tạo tài khoản Admin",
    icon: UserAddOutlined,
    linkTo: "/create-admin",
    admin: true,
  },
];

export const BASE_URL = import.meta.env.VITE_APP_BASE_API_URL;
