import { Redirect, Route, useHistory } from "react-router-dom";
import { AuthStoreType } from "../../auth/utils/types";
import { useSelector } from "react-redux";

type Props = {
  exact: boolean;
  path: string;
  component: () => JSX.Element;
};

const ProtectedRoute = (props: Props) => {
  const { exact, path, component } = props;

  const history = useHistory();

  const { auth } = useSelector((state: { auth: AuthStoreType }) => state.auth);

  if (!auth.is_superuser && history.location.pathname === path) {
    return <Redirect to="/menu" />;
  }

  return <Route exact={exact} path={path} component={component} />;
};

export default ProtectedRoute;
