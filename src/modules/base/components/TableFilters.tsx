import { Button, Input } from "antd";
import { useState } from "react";
import dayjs, { Dayjs } from "dayjs";
import "dayjs/locale/vi";
import locale from "antd/es/date-picker/locale/vi_VN";
import generatePicker from "antd/es/date-picker/generatePicker";
import dayjsGenerateConfig from "rc-picker/lib/generate/dayjs";
import isoWeek from "dayjs/plugin/isoWeek";
import weekday from "dayjs/plugin/weekday";
import updateLocale from "dayjs/plugin/updateLocale";

import "../assets/scss/tableFilters.scss";

dayjs.extend(isoWeek);
dayjs.extend(weekday);
dayjs.extend(updateLocale);
dayjs.updateLocale("vi", {
  weekStart: 1,
});

const datePickerConfig = dayjsGenerateConfig;
datePickerConfig.locale.getWeek = (locale, value) => {
  const clone = value.clone();
  const result = clone.locale(locale);
  return result.isoWeek();
};

const DatePicker = generatePicker<Dayjs>(datePickerConfig);

type Props = {
  onFilter: (name: string, date: Dayjs | null) => void;
  isShowNameFilter?: boolean;
  isShowDateFilter?: boolean;
  initialValues?: {
    name?: string;
    date?: Dayjs;
  };
};

const TableFilters = (props: Props) => {
  const {
    onFilter,
    initialValues,
    isShowNameFilter = true,
    isShowDateFilter = true,
  } = props;

  const [name, setName] = useState(initialValues?.name || "");
  const [date, setDate] = useState<Dayjs | null>(
    initialValues?.date ? dayjs(initialValues?.date, "DD-MM-YYYY") : null
  );

  const handleNameInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
  };

  const handleDatePickerChange = (date: Dayjs | null) => {
    setDate(date);
  };

  const handleFilterBtnClick = () => {
    onFilter(name, date);
    if (window.deferredPrompt) {
      window.deferredPrompt.prompt();
      window.deferredPrompt.userChoice.then((choiceResult: any) => {
        if (choiceResult.outcome === "accepted") {
          console.log("User accepted the install prompt");
        } else {
          console.log("User dismissed the install prompt");
        }
      });
    }
  };

  return (
    <div className="table-filters">
      {isShowNameFilter && (
        <Input
          style={{ width: "200px", marginRight: "10px" }}
          allowClear
          value={name}
          placeholder="Tìm kiếm theo tên"
          onChange={handleNameInputChange}
        />
      )}
      {isShowDateFilter && (
        <DatePicker
          placeholder="Tìm kiếm theo ngày"
          allowClear
          style={{ width: "200px", marginRight: "10px" }}
          format={"DD-MM-YYYY"}
          value={date}
          locale={locale}
          onChange={handleDatePickerChange}
        />
      )}
      <Button type="primary" onClick={handleFilterBtnClick}>
        Lọc
      </Button>
    </div>
  );
};

export default TableFilters;
