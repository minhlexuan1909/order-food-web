import moment from "moment";

const TableCellCreatedDate = (utcTime: string, className?: string) => {
  return (
    <div className={`table-cell-created-date ${className}`}>
      <p>{moment(utcTime).format("DD-MM-YYYY")}</p>
    </div>
  );
};

export default TableCellCreatedDate;
