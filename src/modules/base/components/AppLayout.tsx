import "../assets/scss/appLayout.scss";

import { Layout, Menu } from "antd";
import { Content, Header } from "antd/es/layout/layout";
import Sider from "antd/es/layout/Sider";
import { CSSProperties, useEffect, useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";

import { TAB_INFO } from "../utils/constants";
import { LogoutOutlined, UserOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../common/redux/actions";
import { AuthStoreType } from "../../auth/utils/types";

const AppLayout = (props: any) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const { auth } = useSelector((state: { auth: AuthStoreType }) => state.auth);

  const [isCollapsed, setIsCollapsed] = useState(false);

  const [contentLayoutStyle, setContentLayoutStyle] = useState<
    CSSProperties | undefined
  >({
    position: "absolute",
    height: "calc(100vh - 64px)",
    width: `calc(100vw - ${isCollapsed ? "80px" : "200px"})`,
    right: "0",
    bottom: "0",
  });

  const currentSelectedTabKey =
    TAB_INFO.findIndex((tab) => tab.linkTo === location.pathname) + "" || "0";

  const handleOnCollapse = (collapse: boolean) => {
    setIsCollapsed(collapse);
  };

  const handleLogout = () => {
    dispatch(logout());
    history.push("/auth");
  };

  const handleOnBreakpoint = (broken: boolean) => {
    setIsCollapsed(broken);
  };

  useEffect(() => {
    setContentLayoutStyle(undefined);
  }, []);
  if (location.pathname === "/auth") {
    return <></>;
  }
  return (
    <Layout className="app-layout">
      <Header className="app-layout-header">
        <Link to="/menu" className="logo-text">
          MinhLX
        </Link>
      </Header>
      <Layout>
        <Sider
          theme="light"
          collapsible
          breakpoint="lg"
          onBreakpoint={handleOnBreakpoint}
          collapsed={isCollapsed}
          onCollapse={handleOnCollapse}
          className="app-layout-sider"
        >
          <Menu
            theme="light"
            selectedKeys={[currentSelectedTabKey]}
            mode="inline"
            className="app-layout-sider-menu"
          >
            {TAB_INFO.map((tab, index) => {
              if (!tab.admin || (tab.admin && auth?.is_superuser)) {
                return (
                  <Menu.Item key={index + ""}>
                    <tab.icon />
                    <span>{tab.name}</span>
                    <Link to={tab.linkTo} />
                  </Menu.Item>
                );
              }
            })}
          </Menu>
          <Menu
            theme="light"
            mode="inline"
            selectedKeys={location.pathname === "/profile" ? ["2"] : undefined}
            className="app-layout-sider-menu"
          >
            <Menu.Item key={2}>
              <UserOutlined />
              <span>Trang cá nhân</span>
              <Link to="/profile" />
            </Menu.Item>
            <Menu.Item key={1} onClick={handleLogout}>
              <LogoutOutlined />
              <span>Đăng xuất</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Content style={contentLayoutStyle}>{props.children}</Content>
      </Layout>
    </Layout>
  );
};

export default AppLayout;
