import { COMPONENT_STATES } from "./constants";

export interface IAction<T> {
  type: string | symbol;
  response?: T;
}

export interface IActionRequest<T> {
  type: string | symbol;
  params?: T;
}

export interface IResponse<T> {
  status: number;
  data: T;
}

export interface IPagination<T> {
  count: number;
  next: number | string;
  previous: number | string;
  results: T[];
}

export type ComponentStateType =
  (typeof COMPONENT_STATES)[keyof typeof COMPONENT_STATES];
