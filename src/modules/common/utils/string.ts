export const removeVietnameseTones = (str: string) => {
  try {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    str = str.replace(/ + /g, " ");
    return str;
  } catch (error) {
    return "";
  }
};

export const equalsIgnoreCase = (left: any, right: any) => {
  return left.localeCompare(right, undefined, { sensitivity: "base" }) === 0;
};

export const genRanHex = (size: any) =>
  [...Array(size)].map(() => Math.floor(Math.random() * 16).toString(16)).join("");

export const replaceRawText = (text: string, noSpam?: boolean) => {
  if (!text) {
    return "";
  }
  if (text.length <= 1) {
    return text;
  }
  let result = "";
  let textSplits = text.split(" ");
  if (!text.includes("<span>") && !noSpam) {
    textSplits = textSplits.map((value) => {
      return value.length > 8 ? "" : value;
    });
  }
  textSplits.forEach((txt) => {
    const rawCheck = rawList.find((rawTxt) => txt.toUpperCase().includes(rawTxt.toUpperCase()));
    let replaceTxt = "";
    if (rawCheck) {
      const rawIndex = txt.toUpperCase().indexOf(rawCheck.toUpperCase());
      for (let idx = 0; idx < txt.length; idx++) {
        if (idx >= rawIndex && idx < rawCheck.length + rawIndex) {
          replaceTxt += "*";
        } else {
          replaceTxt += txt[idx];
        }
      }
    } else {
      replaceTxt = txt;
    }
    result += replaceTxt ? `${replaceTxt} ` : "";
  });

  return result;
};

const rawList = [
  "nứng",
  "địt",
  "cặc",
  "buồi",
  "lồn",
  "cửng",
  "vcl",
  "dkm",
  "kẹc",
  "đm",
  "dm",
  "dit",
  "dcm",
  "đệch",
  "mịa",
  "đcm",
  "sextoy",
  "cứt",
  "vkl",
  "bitch",
  "đĩ",
  "đụ",
  "chịch",
  "dái",
  "đuỵt",
  "đệt",
  "fuck",
  "địt",
  "đụ",
  "bìu",
  "phịt",
  "boài",
  "cặc",
  "nứng",
  "moẹ",
  "đít",
  "chịch",
  "lìn",
  "thiến",
  "dú",
  "phịch",
  "vếu",
  "lon ",
  "đéo",
  "vú",
];
