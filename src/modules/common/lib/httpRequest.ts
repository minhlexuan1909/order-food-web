import axios from "axios";
import fetchAdapter from "@vespaiach/axios-fetch-adapter";

const DEFAULT_TIME_OUT = 10 * 1000;
const defaults = {
  headers: {
    "Content-Type": "application/json",
  },
  error: {
    code: "INTERNAL_ERROR",
    message:
      "Something went wrong. Please check your internet connection or contact our support.",
    status: 503,
    data: {},
  },
  baseURL: import.meta.env.VITE_APP_BASE_API_URL,
  timeout: DEFAULT_TIME_OUT,
};

const forceLogout = () => {
  localStorage.clear();
  axios.defaults.headers.common["Authorization"] = "";
  if (!window.location.pathname.includes("auth")) {
    window.location.href = "/auth";
  }
};

const api = (method: any, url: any, variables: any) => {
  return new Promise((resolve, reject) => {
    axios({
      adapter: fetchAdapter,
      url,
      method,
      params: method === "get" ? variables : undefined,
      data: method !== "get" ? variables : undefined,
      ...defaults,
    })
      .then((response: any) => {
        if (response && response.status === STATUS_CODE.AUTHENTICATE) {
          forceLogout();
        }
        resolve(response.data);
      })
      .catch((error: any) => {
        if (error.response) {
          if (error.response.status === STATUS_CODE.AUTHENTICATE) {
            forceLogout();
          }
          reject(error);
        } else {
          reject(defaults.error);
        }
      });
  });
};

export const STATUS_CODE = {
  AUTHENTICATE: 401,
  NOT_FOUND: 404,
};

export const initApi = (token: any) => {
  if (token) axios.defaults.headers.common["Authorization"] = `Token ${token}`;
  axios.defaults.baseURL = import.meta.env.VITE_APP_BASE_API_URL;
};

export const Api = {
  get: (url: any, variables: any): Promise<any> => api("get", url, variables),
  post: (url: any, variables?: any): Promise<any> =>
    api("post", url, variables),
  put: (url: any, variables: any): Promise<any> => api("put", url, variables),
  patch: (url: any, variables: any): Promise<any> =>
    api("patch", url, variables),
  delete: (url: any, variables: any): Promise<any> =>
    api("delete", url, variables),
};
