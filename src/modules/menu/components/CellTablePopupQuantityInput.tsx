import { Form, InputNumber } from "antd";
import React from "react";
import { FoodInForm } from "../utils/types";

const CellTablePopupQuantityInput = (
  record: FoodInForm,
  setFoodsInMenu: React.Dispatch<React.SetStateAction<FoodInForm[]>>
) => {
  const handleInputChange = (value: number | string | null) => {
    setFoodsInMenu((prev) =>
      prev.map((food) => {
        if (food.food_id === record.food_id) {
          return {
            ...food,
            quantity: Number(value),
          };
        }
        return food;
      })
    );
  };

  return (
    <Form.Item
      name={`quantity-${record.food_id}`}
      style={{ margin: 0 }}
      rules={[{ required: true, message: "Vui lòng nhập số lượng" }]}
    >
      <InputNumber
        type="number"
        onChange={handleInputChange}
        step={1}
        min={1}
        placeholder="Nhập số lượng"
        autoComplete={undefined}
      />
    </Form.Item>
  );
};

export default CellTablePopupQuantityInput;
