import { BASE_URL } from "../../base/utils/constants";
import { FoodMenu } from "../utils/types";

import "../assets/scss/foodMenuImage.scss";

const FoodMenuImage = (_: string, record: FoodMenu) => {
  return record?.food?.image_server_path ? (
    <img
      className="food-menu-image"
      src={`${BASE_URL}${record.food.image_server_path}`}
      loading="lazy"
    />
  ) : (
    <div className="food-menu-image"></div>
  );
};

export default FoodMenuImage;
