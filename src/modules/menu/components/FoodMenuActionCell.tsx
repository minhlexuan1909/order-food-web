import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button } from "antd";
import store from "../../common/redux/store";
import { setMenu } from "../redux/actions";
import { Menu } from "../utils/types";

const FoodMenuActionCell = (_: string, record: Menu) => {
  const handleEditBtnClick = () => {
    store.dispatch(
      setMenu({
        isShowActionMenuModal: true,
        action: "EDIT",
        currentActionMenu: record,
      })
    );
  };

  const handleDeleteBtnClick = () => {
    store.dispatch(
      setMenu({ isShowDeleteMenuModal: true, currentActionMenu: record })
    );
  };

  return (
    <div className="food-menu-action-cell">
      <Button style={{ marginRight: "10px" }} onClick={handleEditBtnClick}>
        <EditOutlined />
      </Button>
      <Button type="primary" danger onClick={handleDeleteBtnClick}>
        <DeleteOutlined />
      </Button>
    </div>
  );
};

export default FoodMenuActionCell;
