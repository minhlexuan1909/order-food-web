import { Button, Table, Tooltip } from "antd";
import "../assets/scss/menuWrapper.scss";
import { useDispatch, useSelector } from "react-redux";
import {
  FoodMenu,
  GetMenusQueryRequest,
  Menu,
  MenuStoreType,
} from "../utils/types";
import { useEffect, useState } from "react";
import { getMenus, setMenu } from "../redux/actions";
import { FOOD_MENU_COLUMNS, MENU_COLUMNS } from "../utils/consts";
import { useQuery } from "../../base";
import { PlusCircleOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import ModalActionMenu from "./ModalActionMenu";
import ModalDeleteMenu from "./ModalDeleteMenu";
import ModalOrderMenu from "./ModalOrderMenu";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { AuthStoreType } from "../../auth/utils/types";
import moment from "moment";
import TableFilters from "../../base/components/TableFilters";
import { Dayjs } from "dayjs";

const MenuWrapper = () => {
  const dispatch = useDispatch();
  const { query, handleQuery } = useQuery<GetMenusQueryRequest>();
  const { page, name, date } = query;

  const { auth } = useSelector((state: { auth: AuthStoreType }) => state.auth);
  const { menu } = useSelector((state: { menu: MenuStoreType }) => state.menu);

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [queryCount, setQueryCount] = useState(0);

  const currentHour = Number(moment().utc().utcOffset("+07:00").format("H"));
  const isCanOrder = currentHour >= 8 && currentHour <= 10;

  const handleGetFoodInMenuKey = (record: FoodMenu) => {
    return record.food.id;
  };

  const handlePageChange = (page: number) => {
    handleQuery({ page });
  };

  const handleCreateMenuBtnClick = () => {
    dispatch(setMenu({ isShowActionMenuModal: true }));
  };

  const handleCreateOrderBtnClick = () => {
    dispatch(setMenu({ isShowOrderMenuModal: true }));
  };

  const handleSelectRow = (record: Menu) => {
    if (selectedRowKeys.findIndex((row) => row === record.id) !== -1) {
      setSelectedRowKeys((prev) => prev.filter((row) => row !== record.id));
    } else {
      setSelectedRowKeys((prev) => [...prev, record.id]);
    }
  };

  const handleRenderCell = (
    checked: boolean,
    record: Menu,
    index: number,
    originNode: React.ReactNode
  ) => {
    const isSelectable =
      moment(record.created_at).format("YYYY-MM-DD") ===
        moment().format("YYYY-MM-DD") && isCanOrder;

    if (isSelectable) {
      return originNode;
    } else {
      return <></>;
    }
  };

  const handleTableFilter = (name: string, date: Dayjs | null) => {
    handleQuery({
      page: 1,
      ...(name && { name }),
      ...(date && { date: date.format("DD-MM-YYYY") }),
    });
    setQueryCount((prev) => prev + 1);
  };

  useEffect(() => {
    if (!page) {
      handleQuery({ page: 1 });
    }
  }, []);

  useEffect(() => {
    dispatch(getMenus({ ...query, page: page || 1 }));
  }, [page, name, date, queryCount]);

  return (
    <>
      <div className="menu-wrapper">
        <div className="action-wrapper">
          {auth.is_superuser && (
            <Button
              type="default"
              onClick={handleCreateMenuBtnClick}
              className="btn-add-menu"
            >
              <PlusCircleOutlined /> Thêm thực đơn
            </Button>
          )}
          <Tooltip
            title="Ngoài giờ đặt thực đơn"
            open={isCanOrder ? false : undefined}
          >
            <Button
              className="btn-order-menu"
              type="primary"
              onClick={handleCreateOrderBtnClick}
              disabled={!isCanOrder}
            >
              <ShoppingCartOutlined /> Đặt thực đơn
            </Button>
          </Tooltip>
        </div>

        <TableFilters
          initialValues={{ name, date }}
          onFilter={handleTableFilter}
        />

        <Table
          loading={menu.state === COMPONENT_STATES.LOADING}
          rowSelection={{
            selectedRowKeys,
            onSelect: handleSelectRow,
            hideSelectAll: true,
            renderCell: handleRenderCell,
          }}
          columns={
            auth.is_superuser
              ? MENU_COLUMNS
              : MENU_COLUMNS.slice(0, MENU_COLUMNS.length - 1)
          }
          dataSource={menu.data}
          scroll={{ y: "calc(100vh - 350px)", x: "900px" }}
          pagination={{
            total: menu.total,
            current: Number(page) || 1,
            pageSize: 10,
            showSizeChanger: false,
            onChange: handlePageChange,
          }}
          rowKey={"id"}
          expandable={{
            expandedRowRender: (record) => (
              <Table
                scroll={{ y: "calc(100vh - 500px)", x: "500px" }}
                columns={FOOD_MENU_COLUMNS}
                dataSource={record.food_menu}
                pagination={false}
                rowKey={handleGetFoodInMenuKey}
              />
            ),
          }}
        />
      </div>
      <ModalActionMenu />
      <ModalDeleteMenu />
      <ModalOrderMenu
        selectedIds={selectedRowKeys}
        setSelectedIds={setSelectedRowKeys}
      />
    </>
  );
};

export default MenuWrapper;
