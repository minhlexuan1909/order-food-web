import React from "react";
import { TableMenuDetail } from "../utils/types";
import { Form, InputNumber } from "antd";

const CellTableOrderQuantityInput = (
  record: TableMenuDetail,
  setMenuDetail: React.Dispatch<React.SetStateAction<TableMenuDetail[]>>
) => {
  const handleInputChange = (value: number | string | null) => {
    setMenuDetail((prev) =>
      prev.map((menu) => {
        if (menu.menu_id === record.menu_id) {
          return {
            ...menu,
            quantity: Number(value),
          };
        }
        return menu;
      })
    );
  };

  return (
    <Form.Item
      name={`quantity-${record.menu_id}`}
      style={{ margin: 0 }}
      rules={[{ required: true, message: "Vui lòng nhập số lượng" }]}
    >
      <InputNumber
        type="number"
        onChange={handleInputChange}
        step={1}
        min={1}
        placeholder="Nhập số lượng"
        autoComplete={undefined}
      />
    </Form.Item>
  );
};

export default CellTableOrderQuantityInput;
