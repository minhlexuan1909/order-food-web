import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { MenuStoreType } from "../utils/types";
import { deleteMenu, setMenu } from "../redux/actions";

const ModalDeleteMenu = () => {
  const dispatch = useDispatch();

  const { isShowDeleteMenuModal, currentActionMenu } = useSelector(
    (state: { menu: MenuStoreType }) => state.menu.menu
  );

  const handleOkModal = () => {
    if (currentActionMenu?.id) {
      dispatch(deleteMenu(currentActionMenu?.id));
    }
  };

  const handleModalCancel = () => {
    dispatch(
      setMenu({
        isShowDeleteMenuModal: false,
      })
    );
  };

  return (
    <Modal
      centered
      open={isShowDeleteMenuModal}
      onOk={handleOkModal}
      onCancel={handleModalCancel}
      title={`Xóa thực đơn ${currentActionMenu?.name || ""}`}
      okText="Xác nhận"
      cancelText="Hủy"
    >
      Bạn có chắc chắn muốn xóa thực đơn này?
    </Modal>
  );
};

export default ModalDeleteMenu;
