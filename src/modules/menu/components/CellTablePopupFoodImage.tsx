import { BASE_URL } from "../../base/utils/constants";
import { FoodInForm } from "../utils/types";

import "../assets/scss/cellTablePopupFoodImage.scss";

const CellTablePopupFoodImage = (_: string, record: FoodInForm) => {
  return record.image_server_path ? (
    <img
      className="cell-table-popup-food-image"
      src={`${BASE_URL}${record.image_server_path}`}
      loading="lazy"
    />
  ) : (
    <div className="cell-table-popup-food-image"></div>
  );
};

export default CellTablePopupFoodImage;
