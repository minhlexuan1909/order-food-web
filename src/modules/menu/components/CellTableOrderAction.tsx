import { DeleteOutlined } from "@ant-design/icons";
import { Button } from "antd";
import React from "react";
import { TableMenuDetail } from "../utils/types";

const CellTableOrderAction = (
  record: TableMenuDetail,
  setSelectedIds: React.Dispatch<React.SetStateAction<React.Key[]>>,
  setMenuDetail: React.Dispatch<React.SetStateAction<TableMenuDetail[]>>
) => {
  const handleDeleteBtnClick = () => {
    setSelectedIds((prev) => prev.filter((id) => id !== record.menu_id));
    setMenuDetail((prev) =>
      prev.filter((menu) => menu.menu_id !== record.menu_id)
    );
  };

  return (
    <Button type="primary" danger onClick={handleDeleteBtnClick}>
      <DeleteOutlined />
    </Button>
  );
};

export default CellTableOrderAction;
