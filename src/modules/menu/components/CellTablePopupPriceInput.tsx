import { Form, InputNumber } from "antd";
import { FoodInForm } from "../utils/types";
import React from "react";

const CellTablePopupPriceInput = (
  record: FoodInForm,
  setFoodsInMenu: React.Dispatch<React.SetStateAction<FoodInForm[]>>
) => {
  const handleInputChange = (value: number | string | null) => {
    setFoodsInMenu((prev) =>
      prev.map((food) => {
        if (food.food_id === record.food_id) {
          return {
            ...food,
            unit_price: Number(value),
          };
        }
        return food;
      })
    );
  };

  return (
    <Form.Item
      name={`unit_price-${record.food_id}`}
      style={{ margin: 0 }}
      rules={[{ required: true, message: "Vui lòng nhập giá" }]}
    >
      <InputNumber
        type="number"
        controls={false}
        onChange={handleInputChange}
        placeholder="Nhập giá"
        autoComplete={undefined}
      />
    </Form.Item>
  );
};

export default CellTablePopupPriceInput;
