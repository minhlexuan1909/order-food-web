import { Food } from "../utils/types";
import "../assets/scss/selectFoodOption.scss";
import { BASE_URL } from "../../base/utils/constants";

type Props = {
  food: Food;
};

const SelectFoodOption = (props: Props) => {
  const { food } = props;

  return (
    <div className="class-food-option">
      {food.image_server_path ? (
        <img
          className="food-img"
          src={`${BASE_URL}${food.image_server_path}`}
          loading="lazy"
        />
      ) : (
        <div className="food-img" />
      )}
      <p className="food-name">{food.name}</p>
    </div>
  );
};

export default SelectFoodOption;
