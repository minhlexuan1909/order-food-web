import { useDispatch, useSelector } from "react-redux";
import { MenuStoreType, TableMenuDetail } from "../utils/types";
import { Form, Modal, Table } from "antd";
import {
  createBills,
  searchMenus,
  setMenu,
  setMenuSearch,
} from "../redux/actions";
import { useEffect, useState } from "react";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { Loading3QuartersOutlined } from "@ant-design/icons";
import { getColumnsOrderModal } from "../utils/functions";
import { useForm } from "antd/es/form/Form";

type Props = {
  selectedIds: React.Key[];
  setSelectedIds: React.Dispatch<React.SetStateAction<React.Key[]>>;
};

const ModalOrderMenu = (props: Props) => {
  const { selectedIds, setSelectedIds } = props;

  const dispatch = useDispatch();

  const { isShowOrderMenuModal } = useSelector(
    (state: { menu: MenuStoreType }) => state.menu.menu
  );
  const { data, state } = useSelector(
    (state: { menu: MenuStoreType }) => state.menu.menuSearch
  );

  const [form] = useForm();

  const [menuDetails, setMenuDetai] = useState<TableMenuDetail[]>([]);

  const handleOkModal = () => {
    form.submit();
  };

  const handleCancelModal = () => {
    dispatch(setMenu({ isShowOrderMenuModal: false }));
  };

  const handleOnFinish = (_: any) => {
    dispatch(
      createBills({
        menu_details: menuDetails.map((menu) => ({
          menu_id: menu.menu_id,
          quantity: menu.quantity,
        })),
      })
    );
  };

  useEffect(() => {
    if (isShowOrderMenuModal && selectedIds.length > 0) {
      dispatch(searchMenus({ idList: selectedIds.join(",") }));
    } else {
      dispatch(setMenuSearch({ data: [] }));
    }
  }, [isShowOrderMenuModal, selectedIds]);

  useEffect(() => {
    setMenuDetai(
      data.map((menu) => ({
        menu_id: menu.id,
        name: menu.name,
        quantity: 1,
        total_price: menu.total_price,
      }))
    );
  }, [data]);

  useEffect(() => {
    menuDetails.forEach((menu) => {
      form.setFieldsValue({
        [`quantity-${menu.menu_id}`]: menu.quantity,
      });
    });
  }, [menuDetails]);

  return (
    <Modal
      title="Đặt thực đơn"
      onOk={handleOkModal}
      okText="Xác nhận"
      onCancel={handleCancelModal}
      cancelText="Hủy"
      open={isShowOrderMenuModal}
    >
      {state === COMPONENT_STATES.LOADING && (
        <Loading3QuartersOutlined spin={true} />
      )}
      {state !== COMPONENT_STATES.LOADING && (
        <Form form={form} onFinish={handleOnFinish}>
          <Table
            dataSource={menuDetails}
            columns={getColumnsOrderModal(setSelectedIds, setMenuDetai)}
            rowKey={"menu_id"}
            scroll={{ x: 600, y: "calc(100vh - 400px)" }}
            pagination={false}
          />
        </Form>
      )}
    </Modal>
  );
};

export default ModalOrderMenu;
