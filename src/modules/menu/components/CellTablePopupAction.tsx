import { DeleteOutlined } from "@ant-design/icons";
import { Button } from "antd";
import React from "react";
import { FoodInForm } from "../utils/types";

const CellTablePopupAction = (
  record: FoodInForm,
  setFoodsInMenu: React.Dispatch<React.SetStateAction<FoodInForm[]>>
) => {
  const handleDeleteBtnClick = () => {
    setFoodsInMenu((prev) =>
      prev.filter((food) => food.food_id !== record.food_id)
    );
  };

  return (
    <Button type="primary" danger onClick={handleDeleteBtnClick}>
      <DeleteOutlined />
    </Button>
  );
};

export default CellTablePopupAction;
