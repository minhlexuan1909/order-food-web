import { Form, Input, Modal, Select, Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { FoodInForm, MenuStoreType } from "../utils/types";
import { createMenu, getFoods, setMenu, updateMenu } from "../redux/actions";
import { useForm } from "antd/es/form/Form";
import { useState, useEffect } from "react";
import SelectFoodOption from "./SelectFoodOption";
import { getColumnsFoodModal } from "../utils/functions";
import { COMPONENT_STATES } from "../../common/utils/constants";

import "../assets/scss/modalActionMenu.scss";
import { removeVietnameseTones } from "../../common";

let searchTimeout: null | ReturnType<typeof setTimeout> = null;

const ModalActionMenu = () => {
  const dispatch = useDispatch();

  const { isShowActionMenuModal, action, currentActionMenu } = useSelector(
    (state: { menu: MenuStoreType }) => state.menu.menu
  );
  const { foodSearch } = useSelector(
    (state: { menu: MenuStoreType }) => state.menu
  );

  const [form] = useForm();

  const [foodsInMenu, setFoodsInMenu] = useState<FoodInForm[]>([]);

  const selectOptions = foodSearch.data.map((food) => ({
    food,
    value: food.name,
    label: <SelectFoodOption food={food} />,
  }));

  const handleCancelModal = () => {
    dispatch(setMenu({ isShowActionMenuModal: false, action: "CREATE" }));
  };

  const handleOkModal = () => {
    form.submit();
  };

  const handleFinishForm = (values: any) => {
    const { name } = values;
    if (action === "CREATE") {
      dispatch(
        createMenu({
          name,
          food_in_menu: foodsInMenu.map((food) => ({
            food_id: food.food_id,
            quantity: food.quantity,
            unit_price: food.unit_price,
          })),
        })
      );
    } else {
      if (currentActionMenu) {
        dispatch(
          updateMenu(currentActionMenu?.id, {
            name,
            food_in_menu: foodsInMenu.map((food) => ({
              food_id: food.food_id,
              quantity: food.quantity,
              unit_price: food.unit_price,
            })),
          })
        );
      }
    }
  };

  const handleSelectSearch = (value: string) => {
    if (searchTimeout) {
      clearTimeout(searchTimeout);
    }
    searchTimeout = setTimeout(() => {
      if (value) {
        dispatch(getFoods({ name: value }));
      }
    }, 1000);
  };

  const handleOnSelect = (_: any, option: any) => {
    if (
      foodsInMenu.findIndex((food) => food.food_id === option.food.id) === -1
    ) {
      setFoodsInMenu((prev) => [
        ...prev,
        {
          food_id: option.food.id,
          image: option.food.image,
          image_server_path: option.food.image_server_path,
          name: option.food.name,
          unit_price: option.food.default_price,
          quantity: 1,
        },
      ]);
    }
  };

  useEffect(() => {
    if (currentActionMenu) {
      form.setFieldsValue({
        name: currentActionMenu.name,
      });
      setFoodsInMenu(
        currentActionMenu.food_menu.map((foodMenu) => ({
          food_id: foodMenu.food.id,
          image: foodMenu.food.image,
          image_server_path: foodMenu.food.image_server_path,
          name: foodMenu.food.name,
          quantity: foodMenu.quantity,
          unit_price: foodMenu.unit_price,
        }))
      );
    }
  }, [currentActionMenu]);

  useEffect(() => {
    foodsInMenu.forEach((food) => {
      form.setFieldValue(`quantity-${food.food_id}`, food.quantity);
      form.setFieldValue(`unit_price-${food.food_id}`, food.unit_price);
    });
  }, [foodsInMenu]);

  return (
    <Modal
      width={900}
      open={isShowActionMenuModal}
      onCancel={handleCancelModal}
      onOk={handleOkModal}
      centered
      destroyOnClose
      title={`${
        action === "CREATE"
          ? "Thêm thực đơn"
          : `Sửa thực đơn ${currentActionMenu?.name || ""}`
      } `}
      okText={`${action === "CREATE" ? "Thêm" : "Sửa"} thực đơn`}
      cancelText="Hủy"
      className="modal-action-menu"
    >
      <Form onFinish={handleFinishForm} form={form}>
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Vui lòng nhập tên thực đơn" }]}
        >
          <Input autoComplete="menuName" placeholder="Tên thực đơn" />
        </Form.Item>

        <Select
          placeholder="Tìm kiếm món ăn"
          allowClear
          showSearch
          style={{ marginBottom: "10px", width: "250px" }}
          loading={foodSearch.state === COMPONENT_STATES.LOADING}
          filterOption={(inputValue, option) => {
            return (
              removeVietnameseTones(option?.food.name || "")
                .toLowerCase()
                .includes(removeVietnameseTones(inputValue.toLowerCase())) ||
              false
            );
          }}
          optionLabelProp="value"
          onSelect={handleOnSelect}
          onSearch={handleSelectSearch}
          options={selectOptions}
        />
        <Table
          scroll={{ x: 600, y: "calc(100vh - 400px)" }}
          columns={getColumnsFoodModal(setFoodsInMenu)}
          dataSource={foodsInMenu}
          pagination={false}
          rowKey={"food_id"}
        />
      </Form>
    </Modal>
  );
};

export default ModalActionMenu;
