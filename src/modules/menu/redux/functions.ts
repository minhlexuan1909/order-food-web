import { Dispatch } from "redux";
import { IActionRequest } from "../../common";
import {
  CreateBillsRequest,
  CreateModifyMenuRequest,
  GetFoodsQueryRequest,
  GetMenusQueryRequest,
  MenuStoreType,
} from "../utils/types";
import { setFoodSearch, setMenu, setMenuSearch } from "./actions";
import { COMPONENT_STATES } from "../../common/utils/constants";
import {
  apiCreateBills,
  apiCreateMenu,
  apiDeleteMenu,
  apiGetFoods,
  apiGetMenus,
} from "./services";
import { toast } from "react-toastify";
import { apiUpdateMenu } from "./services";

export const handleGetMenus = async (
  dispatch: Dispatch,
  action: IActionRequest<GetMenusQueryRequest>
) => {
  try {
    dispatch(setMenu({ data: [], state: COMPONENT_STATES.LOADING }));
    const res = await apiGetMenus(action.params || {});
    dispatch(
      setMenu({
        data: res.data.results,
        total: res.data.count,
        state: COMPONENT_STATES.SUCCESS,
      })
    );
  } catch (err) {
    dispatch(
      setMenu({
        state: COMPONENT_STATES.LOADING,
      })
    );
  }
};

export const handleSearchMenus = async (
  dispatch: Dispatch,
  action: IActionRequest<GetMenusQueryRequest>
) => {
  try {
    dispatch(setMenuSearch({ state: COMPONENT_STATES.LOADING }));
    const res = await apiGetMenus(action.params || {});
    dispatch(
      setMenuSearch({
        data: res.data.results,
        state: COMPONENT_STATES.SUCCESS,
      })
    );
  } catch (err) {
    dispatch(
      setMenuSearch({
        state: COMPONENT_STATES.LOADING,
      })
    );
  }
};

export const handleCreateMenu = async (
  dispatch: Dispatch,
  action: IActionRequest<CreateModifyMenuRequest>,
  getState: () => { menu: MenuStoreType }
) => {
  try {
    if (action.params) {
      dispatch(setMenu({ state: COMPONENT_STATES.LOADING }));
      const res = await apiCreateMenu(action.params);
      dispatch(
        setMenu({
          data: [res.data, ...getState().menu.menu.data],
          isShowActionMenuModal: false,
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Tạo thực đơn thành công");
    }
  } catch (err) {
    toast.error("Tạo thực đơn thất bại");
    dispatch(
      setMenu({
        state: COMPONENT_STATES.LOADING,
      })
    );
  }
};

export const handleUpdateMenu = async (
  dispatch: Dispatch,
  action: IActionRequest<{ id: number; data: CreateModifyMenuRequest }>,
  getState: () => { menu: MenuStoreType }
) => {
  try {
    if (action.params) {
      dispatch(setMenu({ state: COMPONENT_STATES.LOADING }));
      const res = await apiUpdateMenu(action.params.id, action.params.data);
      dispatch(
        setMenu({
          data: getState().menu.menu.data.map((item) =>
            item.id === action.params?.id ? res.data : item
          ),
          isShowActionMenuModal: false,
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      toast.success("Cập nhật thực đơn thành công");
    }
  } catch (err) {}
};

export const handleDeleteMenu = async (
  dispatch: Dispatch,
  action: IActionRequest<number>,
  getState: () => { menu: MenuStoreType }
) => {
  try {
    if (action.params) {
      dispatch(setMenu({ state: COMPONENT_STATES.LOADING }));
      await apiDeleteMenu(action.params);
      dispatch(
        setMenu({
          data: getState().menu.menu.data.filter(
            (item) => item.id !== action.params
          ),
          state: COMPONENT_STATES.SUCCESS,
          isShowDeleteMenuModal: false,
        })
      );
      toast.success("Xóa thực đơn thành công");
    }
  } catch (err) {
    toast.error("Xóa thực đơn thất bại");
  }
};

export const handleGetFoods = async (
  dispatch: Dispatch,
  action: IActionRequest<GetFoodsQueryRequest>
) => {
  try {
    dispatch(setFoodSearch({ state: COMPONENT_STATES.LOADING }));
    const response = await apiGetFoods(action.params || {});
    dispatch(
      setFoodSearch({
        data: response.data.results,
        state: COMPONENT_STATES.SUCCESS,
      })
    );
  } catch (err: any) {
    dispatch(
      setFoodSearch({
        state: COMPONENT_STATES.ERROR,
      })
    );
  }
};

export const handleCreateBills = async (
  dispatch: Dispatch,
  action: IActionRequest<CreateBillsRequest>
) => {
  try {
    if (action.params) {
      await apiCreateBills(action.params);
      dispatch(setMenu({ isShowOrderMenuModal: false }));
      toast.success("Đặt thành công");
    }
  } catch (err: any) {
    const statusCode = err.response?.status;
    if (statusCode === 403) {
      toast.error("Ngoài giờ có thể đặt thực đơn");
    }
  }
};
