import {
  CreateBillsRequest,
  CreateModifyMenuRequest,
  FoodSearchReducer,
  GetFoodsQueryRequest,
  GetMenusQueryRequest,
  MenuReducer,
  MenuSearchReducer,
  MenuStoreType,
} from "../utils/types";
import { IAction, IActionRequest } from "../../common/utils/types";
import { ACTION_TYPES } from "./actionTypes";

export const getMenus = (
  params?: GetMenusQueryRequest
): IActionRequest<GetMenusQueryRequest> => {
  return {
    type: ACTION_TYPES.GET_MENNUS,
    params,
  };
};

export const searchMenus = (
  params?: GetMenusQueryRequest
): IActionRequest<GetMenusQueryRequest> => {
  return {
    type: ACTION_TYPES.SEARCH_MENUS,
    params,
  };
};

export const createMenu = (
  params: CreateModifyMenuRequest
): IActionRequest<CreateModifyMenuRequest> => {
  return {
    type: ACTION_TYPES.CREATE_MENU,
    params,
  };
};

export const updateMenu = (
  id: number,
  data: CreateModifyMenuRequest
): IActionRequest<{ id: number; data: CreateModifyMenuRequest }> => {
  return {
    type: ACTION_TYPES.UPDATE_MENU,
    params: { id, data },
  };
};

export const deleteMenu = (id: number): IActionRequest<number> => {
  return {
    type: ACTION_TYPES.DELETE_MENU,
    params: id,
  };
};

export const setMenu = (
  response: Partial<MenuReducer>
): IAction<Partial<MenuReducer>> => {
  return {
    type: ACTION_TYPES.SET_MENU,
    response,
  };
};

export const setMenuSearch = (
  response: Partial<MenuSearchReducer>
): IAction<Partial<MenuSearchReducer>> => {
  return {
    type: ACTION_TYPES.SET_MENU_SEARCH,
    response,
  };
};

export const setReducer = (
  response: Partial<MenuStoreType>
): IAction<Partial<MenuStoreType>> => {
  return {
    type: ACTION_TYPES.SET_REDUCER,
    response,
  };
};

export const setFoodSearch = (
  response: Partial<FoodSearchReducer>
): IAction<Partial<FoodSearchReducer>> => {
  return {
    type: ACTION_TYPES.SET_FOOD_SEARCH,
    response,
  };
};

export const getFoods = (
  params?: GetFoodsQueryRequest
): IActionRequest<GetFoodsQueryRequest> => {
  return {
    type: ACTION_TYPES.GET_FOOD,
    params,
  };
};

export const createBills = (
  params: CreateBillsRequest
): IActionRequest<CreateBillsRequest> => {
  return {
    type: ACTION_TYPES.CREATE_BILLS,
    params,
  };
};
