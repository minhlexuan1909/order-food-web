import { IPagination, IResponse } from "../../common";
import { Api } from "../../common/lib/httpRequest";
import {
  Bill,
  CreateBillsRequest,
  CreateModifyMenuRequest,
  Food,
  GetFoodsQueryRequest,
  GetMenusQueryRequest,
  Menu,
} from "../utils/types";

export const apiGetMenus = (
  params: GetMenusQueryRequest | {}
): Promise<IResponse<IPagination<Menu>>> => {
  return Api.get("/api/menus/", params);
};

export const apiCreateMenu = (
  params: CreateModifyMenuRequest
): Promise<IResponse<Menu>> => {
  return Api.post("/api/menus/", params);
};

export const apiUpdateMenu = (
  id: number,
  params: CreateModifyMenuRequest
): Promise<IResponse<Menu>> => {
  return Api.put(`/api/menus/${id}/`, params);
};

export const apiDeleteMenu = (id: number): Promise<undefined> => {
  return Api.delete(`/api/menus/${id}/`, {});
};

export const apiGetFoods = (
  params: GetFoodsQueryRequest
): Promise<IResponse<IPagination<Food>>> => {
  return Api.get("/api/foods/", params);
};

export const apiCreateBills = (
  params: CreateBillsRequest
): Promise<IResponse<Bill[]>> => {
  return Api.post("/api/bills/create-bills/", params);
};
