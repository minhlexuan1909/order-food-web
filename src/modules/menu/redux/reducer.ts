import { IAction } from "../../common";
import ReducerRegistry from "../../common/redux/ReducerRegistry";
import { MenuStoreType } from "../utils/types";
import { ACTION_TYPES } from "./actionTypes";

const initState: MenuStoreType = {
  menu: {
    total: 0,
    currentPage: 1,
    data: [],
    state: null,
    action: "CREATE",
    isShowActionMenuModal: false,
    isShowOrderMenuModal: false,
    isShowDeleteMenuModal: false,
    currentActionMenu: null,
  },
  menuSearch: {
    data: [],
    state: null,
  },
  foodSearch: {
    data: [],
    state: null,
  },
};

const reducer = (state = initState, action: IAction<any>) => {
  switch (action.type) {
    case ACTION_TYPES.SET_MENU: {
      return {
        ...state,
        menu: {
          ...state.menu,
          ...action.response,
        },
      };
    }
    case ACTION_TYPES.SET_MENU_SEARCH: {
      return {
        ...state,
        menuSearch: {
          ...state.menuSearch,
          ...action.response,
        },
      };
    }
    case ACTION_TYPES.SET_REDUCER: {
      return {
        ...state,
        ...action.response,
      };
    }
    case ACTION_TYPES.SET_FOOD_SEARCH: {
      return {
        ...state,
        foodSearch: {
          ...state.foodSearch,
          ...action.response,
        },
      };
    }
    default:
      return state;
  }
};
ReducerRegistry.register("menu", reducer);
