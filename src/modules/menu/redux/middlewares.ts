import { Store } from "redux";

import { IActionRequest } from "../../common";
import MiddlewareRegistry from "../../common/redux/MiddlewareRegistry";
import { ACTION_TYPES } from "./actionTypes";
import {
  handleCreateBills,
  handleCreateMenu,
  handleDeleteMenu,
  handleGetFoods,
  handleGetMenus,
  handleSearchMenus,
  handleUpdateMenu,
} from "./functions";

export const middleware =
  ({ dispatch, getState }: Store) =>
  (next: Function) =>
  async (action: IActionRequest<any>) => {
    next(action);
    switch (action.type) {
      case ACTION_TYPES.GET_MENNUS: {
        return await handleGetMenus(dispatch, action);
      }
      case ACTION_TYPES.SEARCH_MENUS: {
        return await handleSearchMenus(dispatch, action);
      }
      case ACTION_TYPES.CREATE_MENU: {
        return await handleCreateMenu(dispatch, action, getState);
      }
      case ACTION_TYPES.UPDATE_MENU: {
        return await handleUpdateMenu(dispatch, action, getState);
      }
      case ACTION_TYPES.DELETE_MENU: {
        return await handleDeleteMenu(dispatch, action, getState);
      }
      case ACTION_TYPES.GET_FOOD: {
        return await handleGetFoods(dispatch, action);
      }
      case ACTION_TYPES.CREATE_BILLS: {
        return await handleCreateBills(dispatch, action);
      }
    }
  };

MiddlewareRegistry.register(middleware);
