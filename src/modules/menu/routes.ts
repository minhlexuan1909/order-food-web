import MenuPage from "./pages/MenuPage";

export const menuRoutes = [
  {
    path: "/menu",
    component: MenuPage,
    noLayout: false,
  },
];
