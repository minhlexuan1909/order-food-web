import CellTableOrderAction from "../components/CellTableOrderAction";
import CellTableOrderQuantityInput from "../components/CellTableOrderQuantityInput";
import CellTablePopupAction from "../components/CellTablePopupAction";
import CellTablePopupFoodImage from "../components/CellTablePopupFoodImage";
import CellTablePopupPriceInput from "../components/CellTablePopupPriceInput";
import CellTablePopupQuantityInput from "../components/CellTablePopupQuantityInput";
import { FoodInForm, TableMenuDetail } from "./types";

export const getColumnsFoodModal = (
  setFoodsInMenu: React.Dispatch<React.SetStateAction<FoodInForm[]>>
) => {
  return [
    {
      title: "Ảnh",
      dataIndex: "image",
      key: "image",
      render: CellTablePopupFoodImage,
      width: 100,
    },
    {
      title: "Tên món ăn",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Giá",
      dataIndex: "unit_price",
      key: "unit_price",
      render: (_: string, record: FoodInForm) =>
        CellTablePopupPriceInput(record, setFoodsInMenu),
      width: 150,
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
      render: (_: string, record: FoodInForm) =>
        CellTablePopupQuantityInput(record, setFoodsInMenu),
      width: 100,
    },
    {
      title: "",
      render: (_: string, record: FoodInForm) =>
        CellTablePopupAction(record, setFoodsInMenu),
      width: 70,
    },
  ];
};

export const getColumnsOrderModal = (
  setSelectedIds: React.Dispatch<React.SetStateAction<React.Key[]>>,
  setMenuDetail: React.Dispatch<React.SetStateAction<TableMenuDetail[]>>
) => [
  {
    title: "Tên thực đơn",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Giá",
    dataIndex: "total_price",
    key: "total_price",
  },
  {
    title: "Số lượng",
    render: (_: string, record: TableMenuDetail) =>
      CellTableOrderQuantityInput(record, setMenuDetail),
  },
  {
    title: "",
    render: (_: string, record: TableMenuDetail) =>
      CellTableOrderAction(record, setSelectedIds, setMenuDetail),
  },
];
