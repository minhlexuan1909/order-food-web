import { ComponentStateType } from "../../common/utils/types";
export interface MenuStoreType {
  menu: MenuReducer;
  menuSearch: MenuSearchReducer;
  foodSearch: FoodSearchReducer;
}
export interface MenuReducer {
  total: number;
  currentPage: number;
  data: Menu[];
  state: null | ComponentStateType;
  action: "CREATE" | "EDIT";
  isShowActionMenuModal: boolean;
  isShowOrderMenuModal: boolean;
  isShowDeleteMenuModal: boolean;
  currentActionMenu: Menu | null;
}

export interface MenuSearchReducer {
  data: Menu[];
  state: null | ComponentStateType;
}

export interface FoodSearchReducer {
  data: Food[];
  state: null | ComponentStateType;
}

export interface Food {
  id: number;
  name: string;
  image: string;
  image_server_path: string;
  description: string;
  default_price: number;
  created_at: string;
  updated_at: string;
  is_active: boolean;
}

export interface FoodMenu {
  food: Food;
  quantity: number;
  unit_price: number;
}

export interface Menu {
  id: number;
  food_menu: FoodMenu[];
  total_price: number;
  name: string;
  created_at: string;
  updated_at: string;
}

export interface GetMenusQueryRequest {
  [key: string]: any;
  idList?: string;
  page?: number;
  name?: string;
}

export interface FoodMenuRequest {
  food_id: number;
  quantity: number;
  unit_price: number;
}

export interface CreateModifyMenuRequest {
  name: string;
  food_in_menu: FoodMenuRequest[];
}

export interface FoodInForm {
  food_id: number;
  image: string | null;
  image_server_path: string | null;
  name: string;
  unit_price: number;
  quantity: number;
}

export interface GetFoodsQueryRequest {
  [key: string]: any;
  name?: string;
  page?: number;
}

export interface TableMenuDetail {
  menu_id: number;
  name: string;
  total_price: number;
  quantity: number;
}
export interface MenuDetail {
  menu_id: number;
  quantity: number;
}

export interface CreateBillsRequest {
  menu_details: MenuDetail[];
}

export interface User {
  id: number;
  password: string;
  last_login?: any;
  is_superuser: boolean;
  phone: string;
  name: string;
  is_active: boolean;
  is_staff: boolean;
  verify_phone_otp?: any;
  reset_pass_otp?: any;
  created_at: string;
  updated_at: string;
  groups: any[];
  user_permissions: any[];
  menus: Menu[];
}

export interface Food {
  id: number;
  name: string;
  image: string;
  description: string;
  default_price: number;
  created_at: string;
  updated_at: string;
  is_active: boolean;
}

export interface Menu {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  foods: Food[];
}

export interface Bill {
  id: number;
  quantity: number;
  is_paid: boolean;
  created_at: string;
  updated_at: string;
  user: User;
  menu: Menu;
}
