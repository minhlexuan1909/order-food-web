import TableCellCreatedDate from "../../base/components/TableCellCreatedDate";
import CellTablePopupFoodImage from "../components/CellTablePopupFoodImage";
import FoodMenuActionCell from "../components/FoodMenuActionCell";
import FoodMenuImage from "../components/FoodMenuImage";
import { Menu } from "./types";

export const MENU_COLUMNS = [
  {
    title: "ID",
    dataIndex: "id",
    key: "id",
  },
  {
    title: "Tên thực đơn",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Giá",
    dataIndex: "total_price",
    key: "total_price",
  },
  {
    title: "Ngày tạo",
    render: (_: string, record: Menu) =>
      TableCellCreatedDate(record.created_at),
  },
  {
    title: "",
    render: FoodMenuActionCell,
    width: 140,
  },
];

export const FOOD_MENU_COLUMNS = [
  {
    title: "ID",
    dataIndex: ["food", "id"],
    key: "id",
    width: 50,
  },
  {
    title: "Tên món ăn",
    dataIndex: ["food", "name"],
    key: "name",
  },
  {
    title: "Ảnh",
    dataIndex: ["food", "image"],
    key: "image",
    render: FoodMenuImage,
  },
  {
    title: "Mô tả",
    dataIndex: ["food", "description"],
    key: "description",
  },
  {
    title: "Giá",
    dataIndex: "unit_price",
    key: "unit_price",
  },
  {
    title: "Số lượng",
    dataIndex: "quantity",
    key: "quantity",
  },
];
