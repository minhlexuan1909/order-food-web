import React, { Suspense } from "react";

const MenuWrapper = React.lazy(() => import("../components/MenuWrapper"));

const MenuPage = () => {
  return (
    <div>
      <Suspense fallback={<></>}>
        <MenuWrapper />
      </Suspense>
    </div>
  );
};

export default MenuPage;
