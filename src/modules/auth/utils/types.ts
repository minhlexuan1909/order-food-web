import { CarouselRef } from "antd/es/carousel";
import { ComponentStateType } from "../../common";

export interface AuthStoreType {
  carouselRef: CarouselRef | null;
  auth: AuthReducer;
  register: RegisterReducer;
  profile: ProfileReducer;
}

export interface AuthReducer {
  token: string;
  is_superuser: boolean;
  state: null | ComponentStateType;
}

export interface RegisterReducer {
  state: null | ComponentStateType;
}

export interface ProfileReducer extends Profile {
  state: null | ComponentStateType;
}

export interface LoginData {
  phone: string;
  password: string;
}

export interface LoginResponse {
  token: string;
  is_superuser: boolean;
}

export interface RegisterData {
  name: string;
  phone: string;
  password: string;
  password_confirm: string;
}

export interface RegisterResponse {
  phone: string;
  name: string;
  is_staff: boolean;
}

export interface Profile {
  phone: string;
  name: string;
  is_staff: boolean;
  is_superuser: boolean;
}
