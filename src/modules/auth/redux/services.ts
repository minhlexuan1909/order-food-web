import { IResponse } from "../../common";
import { Api } from "../../common/lib/httpRequest";
import {
  LoginData,
  LoginResponse,
  Profile,
  RegisterData,
  RegisterResponse,
} from "../utils/types";

export const apiLogin = (
  params: LoginData
): Promise<IResponse<LoginResponse>> => {
  return Api.post("/api/token/", params);
};

export const apiRegister = (
  params: RegisterData
): Promise<IResponse<RegisterResponse>> => {
  return Api.post("/api/create/", params);
};

export const apiRegisterAdmin = (
  params: RegisterData
): Promise<IResponse<RegisterResponse>> => {
  return Api.post("/api/create-admin/", params);
};

export const apiGetProfile = (): Promise<IResponse<Profile>> => {
  return Api.get("/api/profile/", {});
};

export const apiUpdateProfile = (
  params: RegisterData
): Promise<IResponse<Profile>> => {
  return Api.patch("/api/profile/", params);
};
