import { Dispatch } from "redux";
import { IActionRequest } from "../../common";
import { LoginData, RegisterData } from "../utils/types";
import {
  apiGetProfile,
  apiLogin,
  apiRegister,
  apiRegisterAdmin,
  apiUpdateProfile,
} from "./services";
import { setAuth, setProfile, setRegister } from "./actions";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { toast } from "react-toastify";
import { initApi } from "../../common/lib/httpRequest";

export const handleLogin = async (
  dispatch: Dispatch,
  action: IActionRequest<LoginData>
) => {
  try {
    if (action.params) {
      dispatch(setAuth({ state: COMPONENT_STATES.LOADING }));
      const res = await apiLogin(action.params);
      dispatch(
        setAuth({
          token: res.data.token,
          is_superuser: res.data.is_superuser,
          state: COMPONENT_STATES.SUCCESS,
        })
      );
      initApi(res.data.token);
      toast.success("Đăng nhập thành công");
    }
  } catch (err: any) {
    toast.error("Sai tài khoản hoặc mật khẩu");
    dispatch(setAuth({ state: COMPONENT_STATES.ERROR }));
  }
};

export const handleRegister = async (
  dispatch: Dispatch,
  action: IActionRequest<RegisterData>
) => {
  try {
    if (action.params) {
      dispatch(setRegister({ state: COMPONENT_STATES.LOADING }));
      await apiRegister(action.params);
      dispatch(setRegister({ state: COMPONENT_STATES.SUCCESS }));
      toast.success("Đăng ký tài khoản thành công");
    }
  } catch (err: any) {
    dispatch(
      setRegister({
        state: COMPONENT_STATES.ERROR,
      })
    );
    const statusCode = err.response?.status;
    if (statusCode === 409) {
      toast.error("Số điện thoại đã được sử dụng");
    }
  }
};

export const handleRegisterAdmin = async (
  dispatch: Dispatch,
  action: IActionRequest<RegisterData>
) => {
  try {
    if (action.params) {
      dispatch(setRegister({ state: COMPONENT_STATES.LOADING }));
      await apiRegisterAdmin(action.params);
      dispatch(setRegister({ state: COMPONENT_STATES.SUCCESS }));
      toast.success("Đăng ký tài khoản thành công");
    }
  } catch (err: any) {
    dispatch(
      setRegister({
        state: COMPONENT_STATES.ERROR,
      })
    );
    const statusCode = err.response?.status;
    if (statusCode === 409) {
      toast.error("Số điện thoại đã được sử dụng");
    }
  }
};

export const handleGetProfile = async (dispatch: Dispatch) => {
  try {
    dispatch(setProfile({ state: COMPONENT_STATES.LOADING }));
    const res = await apiGetProfile();
    dispatch(setProfile({ ...res.data, state: COMPONENT_STATES.SUCCESS }));
  } catch (err: any) {}
};

export const handleUpdateProfile = async (
  dispatch: Dispatch,
  action: IActionRequest<RegisterData>
) => {
  try {
    if (action.params) {
      dispatch(setProfile({ state: COMPONENT_STATES.LOADING }));
      const res = await apiUpdateProfile(action.params);
      dispatch(setProfile({ ...res.data, state: COMPONENT_STATES.SUCCESS }));
      toast.success("Cập nhật tài khoản thành công");
    }
  } catch (err: any) {
    dispatch(
      setProfile({
        state: COMPONENT_STATES.ERROR,
      })
    );
    const statusCode = err.response?.status;
    if (statusCode === 409) {
      toast.error("Số điện thoại đã được sử dụng");
    }
  }
};
