import { Store } from "redux";

import { IActionRequest } from "../../common";
import MiddlewareRegistry from "../../common/redux/MiddlewareRegistry";
import { ACTION_TYPES } from "./actionTypes";
import {
  handleGetProfile,
  handleLogin,
  handleRegister,
  handleRegisterAdmin,
  handleUpdateProfile,
} from "./functions";

export const middleware =
  ({ dispatch, getState }: Store) =>
  (next: Function) =>
  async (action: IActionRequest<any>) => {
    next(action);
    switch (action.type) {
      case ACTION_TYPES.LOGIN: {
        return await handleLogin(dispatch, action);
      }
      case ACTION_TYPES.REGISTER: {
        return await handleRegister(dispatch, action);
      }
      case ACTION_TYPES.REGISTER_ADMIN: {
        return await handleRegisterAdmin(dispatch, action);
      }
      case ACTION_TYPES.GET_PROFILE: {
        return await handleGetProfile(dispatch);
      }
      case ACTION_TYPES.UPDATE_PROFILE: {
        return await handleUpdateProfile(dispatch, action);
      }
    }
  };

MiddlewareRegistry.register(middleware);
