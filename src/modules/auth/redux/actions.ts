import { CarouselRef } from "antd/es/carousel";
import { IAction, IActionRequest } from "../../common";
import { ACTION_TYPES } from "./actionTypes";
import {
  AuthReducer,
  LoginData,
  ProfileReducer,
  RegisterData,
  RegisterReducer,
} from "../utils/types";

export const setCarouselRef = (
  carouselRef: CarouselRef | null
): IAction<CarouselRef | null> => {
  return {
    type: ACTION_TYPES.SET_CAROUSET_REF,
    response: carouselRef,
  };
};

export const login = (params: LoginData): IActionRequest<LoginData> => {
  return {
    type: ACTION_TYPES.LOGIN,
    params,
  };
};

export const setAuth = (
  response: Partial<AuthReducer>
): IAction<Partial<AuthReducer>> => {
  return {
    type: ACTION_TYPES.SET_AUTH,
    response,
  };
};

export const registerUser = (
  params: RegisterData
): IActionRequest<RegisterData> => {
  return {
    type: ACTION_TYPES.REGISTER,
    params,
  };
};

export const registerAdmin = (
  params: RegisterData
): IActionRequest<RegisterData> => {
  return {
    type: ACTION_TYPES.REGISTER_ADMIN,
    params,
  };
};

export const setRegister = (
  response: Partial<RegisterReducer>
): IAction<Partial<RegisterReducer>> => {
  return {
    type: ACTION_TYPES.SET_AUTH,
    response,
  };
};

export const getProfile = (): IActionRequest<undefined> => {
  return {
    type: ACTION_TYPES.GET_PROFILE,
  };
};

export const setProfile = (
  response: Partial<ProfileReducer>
): IAction<Partial<ProfileReducer>> => {
  return {
    type: ACTION_TYPES.SET_PROFILE,
    response,
  };
};

export const updateProfile = (
  params: Partial<RegisterData>
): IActionRequest<Partial<RegisterData>> => {
  return {
    type: ACTION_TYPES.UPDATE_PROFILE,
    params,
  };
};
