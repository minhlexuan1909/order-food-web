import persistReducer from "redux-persist/es/persistReducer";
import { IAction } from "../../common";
import ReducerRegistry from "../../common/redux/ReducerRegistry";
import { AuthStoreType } from "../utils/types";
import { ACTION_TYPES } from "./actionTypes";
import { ACTION_TYPES as COMMON_ACTION_TYPES } from "../../common/redux/actionTypes";
import storage from "redux-persist/lib/storage";

const initState: AuthStoreType = {
  carouselRef: null,
  auth: {
    token: "",
    is_superuser: false,
    state: null,
  },
  register: {
    state: null,
  },
  profile: {
    phone: "",
    name: "",
    is_staff: false,
    is_superuser: false,
    state: null,
  },
};

const reducer = (state = initState, action: IAction<any>) => {
  switch (action.type) {
    case ACTION_TYPES.SET_CAROUSET_REF:
      return {
        ...state,
        carouselRef: action.response,
      };
    case ACTION_TYPES.SET_AUTH:
      return {
        ...state,
        auth: { ...state.auth, ...action.response },
      };
    case ACTION_TYPES.SET_REGISTER:
      return {
        ...state,
        register: { ...state.auth, ...action.response },
      };
    case ACTION_TYPES.SET_PROFILE:
      return {
        ...state,
        profile: { ...state.profile, ...action.response },
      };

    case COMMON_ACTION_TYPES.LOGOUT: {
      return initState;
    }
    default:
      return state;
  }
};

const authPersistConfig = {
  key: "auth",
  storage: storage,
  whitelist: ["auth"],
};
ReducerRegistry.register("auth", persistReducer(authPersistConfig, reducer));
