import "../assets/scss/authWrapper.scss";

import { Carousel } from "antd";
import { CarouselRef } from "antd/es/carousel";
import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

import SlideAuth from "./SlideAuth";
import { setCarouselRef } from "../redux/actions";
import { AuthStoreType } from "../utils/types";
import { useHistory } from "react-router-dom";

const AuthWrapper = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const carouselRef = useRef<CarouselRef | null>(null);

  const { token } = useSelector(
    (state: { auth: AuthStoreType }) => state.auth.auth
  );

  useEffect(() => {
    if (carouselRef.current) {
      dispatch(setCarouselRef(carouselRef.current));
    }
  }, [carouselRef]);

  useEffect(() => {
    if (token) {
      history.push("/menu");
    }
  }, [token]);

  return (
    <div className="auth-wrapper">
      <Carousel infinite={false} ref={carouselRef} dots={false}>
        <SlideAuth />
        <div className="slide-wrapper">
          <div className="forgot-container container">
            <div className="forgot-password">
              <h1 className="title">Reset Password</h1>
              <div className="text">
                Enter the email associated with your account and we'll send an
                email...
              </div>
              {/* <ForgotPassForm goTo={goTo} /> */}
            </div>
          </div>
        </div>
      </Carousel>
    </div>
  );
};

export default AuthWrapper;
