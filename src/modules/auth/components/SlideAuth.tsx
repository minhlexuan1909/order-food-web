import SlideContainer from "./SlideContainer";
import { Tabs } from "antd";
import TabPane from "antd/es/tabs/TabPane";
import FormLogin from "./FormLogin";

import "../assets/scss/slideAuth.scss";
import FormRegister from "./FormRegister";

const SlideAuth = () => {
  return (
    <div className="slide-auth">
      <SlideContainer>
        <Tabs defaultActiveKey="1" centered type="card" size="large">
          <TabPane tab="Đăng nhập" key="1">
            <FormLogin />
          </TabPane>
          <TabPane tab="Đăng ký" key="2">
            <FormRegister />
          </TabPane>
        </Tabs>
      </SlideContainer>
    </div>
  );
};

export default SlideAuth;
