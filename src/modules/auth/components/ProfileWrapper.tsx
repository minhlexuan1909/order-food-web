import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProfile, updateProfile } from "../redux/actions";
import { Button, Form, Input } from "antd";
import { AuthStoreType, RegisterData } from "../utils/types";
import {
  Loading3QuartersOutlined,
  LockOutlined,
  PhoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { COMPONENT_STATES } from "../../common/utils/constants";

import "../assets/scss/profileWrapper.scss";
import { useForm } from "antd/es/form/Form";

const ProfileWrapper = () => {
  const dispatch = useDispatch();

  const [form] = useForm();

  const { profile } = useSelector(
    (state: { auth: AuthStoreType }) => state.auth
  );

  const isUpdating = profile.state === COMPONENT_STATES.LOADING;

  useEffect(() => {
    dispatch(getProfile());
  }, []);

  useEffect(() => {
    if (profile) {
      form.setFieldsValue({
        name: profile.name,
        phone: profile.phone,
      });
    }
  }, [profile]);

  const handleFinishForm = (values: RegisterData) => {
    dispatch(
      updateProfile({
        name: values.name,
        password: values.password,
        password_confirm: values.password_confirm,
      })
    );
  };

  return (
    <div className="profile-wrapper">
      <Form
        style={{ width: "70%" }}
        onFinish={handleFinishForm}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
        form={form}
      >
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Vui lòng nhập tên của bạn" }]}
        >
          <Input
            autoComplete="name"
            placeholder="Họ và tên"
            prefix={<UserOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="phone"
          rules={[
            { required: true, message: "Vui lòng nhập số điện thoại của bạn" },
          ]}
        >
          <Input
            disabled
            autoComplete="phone"
            placeholder="Số điện thoại"
            prefix={<PhoneOutlined />}
          />
        </Form.Item>
        <Form.Item name="password">
          <Input.Password
            autoComplete="password"
            placeholder="Mật khẩu"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password_confirm"
          rules={[
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (
                  !getFieldValue("password") ||
                  getFieldValue("password") === value
                ) {
                  return Promise.resolve();
                } else
                  return Promise.reject(
                    new Error("Mật khẩu xác nhận không trùng khớp")
                  );
              },
            }),
          ]}
        >
          <Input.Password
            autoComplete="password_confirm"
            placeholder="Mật khẩu xác nhận"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 24 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: "100%" }}
            size="large"
            disabled={isUpdating}
          >
            {isUpdating ? <Loading3QuartersOutlined spin /> : "Lưu thay đổi"}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default ProfileWrapper;
