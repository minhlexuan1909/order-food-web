import {
  Loading3QuartersOutlined,
  LockOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import { useForm } from "antd/es/form/Form";
import { AuthStoreType, LoginData } from "../utils/types";

import "../assets/scss/formLogin.scss";
import { useDispatch, useSelector } from "react-redux";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { login } from "../redux/actions";

const FormLogin = () => {
  const dispatch = useDispatch();

  const { carouselRef, auth } = useSelector(
    (state: { auth: AuthStoreType }) => state.auth
  );

  const isLogin = auth.state === COMPONENT_STATES.LOADING;

  const [form] = useForm<LoginData>();

  const handleFinishForm = (values: LoginData) => {
    dispatch(login(values));
  };

  const handleForgetPasword = () => {
    if (carouselRef) {
      carouselRef.goTo(2);
    }
  };

  return (
    <div className="form-login">
      <Form
        form={form}
        onFinish={handleFinishForm}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
      >
        <Form.Item
          name="phone"
          rules={[
            { required: true, message: "Vui lòng nhập số điện thoại của bạn" },
          ]}
        >
          <Input
            autoComplete="phone"
            placeholder="Số điện thoại"
            prefix={<PhoneOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
        >
          <Input.Password
            autoComplete="password"
            placeholder="Mật khẩu"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 24 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: "100%" }}
            size="large"
            disabled={isLogin}
          >
            {isLogin ? <Loading3QuartersOutlined spin /> : "Đăng nhập"}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default FormLogin;
