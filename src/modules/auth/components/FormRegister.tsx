import { useDispatch, useSelector } from "react-redux";
import "../assets/scss/formRegister.scss";
import { AuthStoreType, RegisterData } from "../utils/types";
import { Button, Form, Input } from "antd";
import { useForm } from "antd/es/form/Form";
import {
  Loading3QuartersOutlined,
  LockOutlined,
  PhoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { COMPONENT_STATES } from "../../common/utils/constants";
import { registerUser } from "../redux/actions";

const FormRegister = () => {
  const dispatch = useDispatch();

  const { register } = useSelector(
    (state: { auth: AuthStoreType }) => state.auth
  );

  const isRegister = register.state === COMPONENT_STATES.LOADING;

  const [form] = useForm<RegisterData>();

  const handleFinishForm = (values: RegisterData) => {
    dispatch(registerUser(values));
  };

  return (
    <div className="form-register">
      <Form
        form={form}
        onFinish={handleFinishForm}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
      >
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Vui lòng nhập tên của bạn" }]}
        >
          <Input
            autoComplete="name"
            placeholder="Họ và tên"
            prefix={<UserOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="phone"
          rules={[
            { required: true, message: "Vui lòng nhập số điện thoại của bạn" },
          ]}
        >
          <Input
            autoComplete="phone"
            placeholder="Số điện thoại"
            prefix={<PhoneOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
        >
          <Input.Password
            autoComplete="password"
            placeholder="Mật khẩu"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password_confirm"
          rules={[
            { required: true, message: "Mật khẩu xác nhận không trùng khớp" },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                } else
                  return Promise.reject(
                    new Error("Mật khẩu xác nhận không trùng khớp")
                  );
              },
            }),
          ]}
        >
          <Input.Password
            autoComplete="password_confirm"
            placeholder="Mật khẩu xác nhận"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 24 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: "100%" }}
            size="large"
            disabled={isRegister}
          >
            {isRegister ? <Loading3QuartersOutlined spin /> : "Đăng ký"}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default FormRegister;
