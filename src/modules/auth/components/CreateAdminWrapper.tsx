import { Button, Form, Input } from "antd";
import { useForm } from "antd/es/form/Form";
import { useDispatch, useSelector } from "react-redux";
import { registerAdmin } from "../redux/actions";
import { AuthStoreType, RegisterData } from "../utils/types";
import {
  Loading3QuartersOutlined,
  LockOutlined,
  PhoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { COMPONENT_STATES } from "../../common/utils/constants";

import "../assets/scss/createAdminWrapper.scss";

const CreateAdminWrapper = () => {
  const dispatch = useDispatch();

  const { register } = useSelector(
    (state: { auth: AuthStoreType }) => state.auth
  );

  const [form] = useForm();

  const isRegister = register.state === COMPONENT_STATES.LOADING;

  const handleFinishForm = (values: RegisterData) => {
    dispatch(registerAdmin(values));
  };
  return (
    <div className="create-admin-wrapper">
      <Form
        style={{ width: "70%" }}
        form={form}
        onFinish={handleFinishForm}
        wrapperCol={{ span: 24 }}
        autoComplete="off"
      >
        <Form.Item
          name="name"
          rules={[{ required: true, message: "Vui lòng nhập tên của bạn" }]}
        >
          <Input
            autoComplete="name"
            placeholder="Họ và tên"
            prefix={<UserOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="phone"
          rules={[
            { required: true, message: "Vui lòng nhập số điện thoại của bạn" },
          ]}
        >
          <Input
            autoComplete="phone"
            placeholder="Số điện thoại"
            prefix={<PhoneOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
        >
          <Input.Password
            autoComplete="password"
            placeholder="Mật khẩu"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password_confirm"
          rules={[
            { required: true, message: "Mật khẩu xác nhận không trùng khớp" },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                } else
                  return Promise.reject(
                    new Error("Mật khẩu xác nhận không trùng khớp")
                  );
              },
            }),
          ]}
        >
          <Input.Password
            autoComplete="password_confirm"
            placeholder="Mật khẩu xác nhận"
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 24 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: "100%" }}
            size="large"
            disabled={isRegister}
          >
            {isRegister ? <Loading3QuartersOutlined spin /> : "Đăng ký"}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CreateAdminWrapper;
