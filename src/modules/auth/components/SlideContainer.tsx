import React from "react";
import "../assets/scss/slideContainer.scss";

type Props = {
  children: React.ReactNode;
};

const SlideContainer = (props: Props) => {
  const { children } = props;
  return (
    <div className="slide-container">
      <div className="container">{children}</div>
    </div>
  );
};

export default SlideContainer;
