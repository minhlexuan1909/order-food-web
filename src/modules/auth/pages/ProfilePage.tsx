import React, { Suspense } from "react";

const ProfileWrapper = React.lazy(() => import("../components/ProfileWrapper"));

const ProfilePage = () => {
  return (
    <Suspense fallback={<></>}>
      <ProfileWrapper />
    </Suspense>
  );
};

export default ProfilePage;
