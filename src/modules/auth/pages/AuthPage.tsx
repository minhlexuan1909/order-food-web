import React, { Suspense } from "react";
const AuthWrapper = React.lazy(() => import("../components/AuthWrapper"));

const AuthPage = () => {
  return (
    <Suspense fallback={<></>}>
      <AuthWrapper />
    </Suspense>
  );
};

export default AuthPage;
