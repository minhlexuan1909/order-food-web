import React, { Suspense } from "react";

const CreateAdminWrapper = React.lazy(
  () => import("../components/CreateAdminWrapper")
);

const CreateAdminPage = () => {
  return (
    <Suspense fallback={<></>}>
      <CreateAdminWrapper />
    </Suspense>
  );
};

export default CreateAdminPage;
