import AuthPage from "./pages/AuthPage";
import CreateAdminPage from "./pages/CreateAdminPage";
import ProfilePage from "./pages/ProfilePage";
export const authRoutes = [
  {
    path: "/auth",
    component: AuthPage,
    noLayout: true,
  },
  {
    path: "/create-admin",
    component: CreateAdminPage,
    noLayout: false,
    adminOnly: true,
  },
  {
    path: "/profile",
    component: ProfilePage,
    noLayout: false,
  },
];
