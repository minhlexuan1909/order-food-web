if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/service-worker.js")
    .then((registration) => {
      console.log("SW registered: ", registration);
    });
}

window.addEventListener("beforeinstallprompt", (event) => {
  console.log("before install prompt fired");
  event.preventDefault();
  window.deferredPrompt = event;
});
