import "./App.scss";
import "./modules/auth";
import "./modules/food";
import "./modules/menu";
import "./modules/history";

import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";

import { AppLayout } from "./modules/base";
import { routes } from "./routes";
import store, { persistor } from "./modules/common/redux/store";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ProtectedRoute from "./modules/base/components/ProtectedRoute";

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ToastContainer
          position="top-right"
          autoClose={1500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="colored"
        />
        <BrowserRouter>
          {routes.map((route) => {
            const PermissionRoute = route.adminOnly ? ProtectedRoute : Route;
            return (
              route.noLayout && (
                <PermissionRoute
                  key={route.path}
                  exact
                  path={route.path}
                  component={route.component}
                />
              )
            );
          })}
          <AppLayout>
            {routes.map((route) => {
              const PermissionRoute = route.adminOnly ? ProtectedRoute : Route;
              return (
                !route.noLayout && (
                  <PermissionRoute
                    key={route.path}
                    exact
                    path={route.path}
                    component={route.component}
                  />
                )
              );
            })}
          </AppLayout>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}

export default App;
