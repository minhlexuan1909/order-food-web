import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { initApi } from "./modules/common/lib/httpRequest.ts";
import "./serviceWorkerRegistration.ts"

const authStore = localStorage.getItem("persist:auth");
if (authStore) {
  try {
    const authStoreJson = JSON.parse(authStore);
    try {
      const authJson = JSON.parse(authStoreJson?.auth);
      if (authJson?.token) {
        initApi(authJson?.token);
      }
    } catch (error) {}
  } catch (error) {}
} else {
  initApi("");
}

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
