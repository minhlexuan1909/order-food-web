import { Redirect } from "react-router-dom";

const TempHomePage = () => {
  return <Redirect to="/menu" />;
};

export default TempHomePage;
