/// <reference lib="webworker" />
/**
 * @type {ServiceWorkerGlobalScope}
 */
const sw = self;

sw.addEventListener("install", (event) => {
  console.log("[SW] Installing SW... ", event);
});

sw.addEventListener("activate", (event) => {
  console.log("[SW] Activating SW...", event);
  return sw.clients.claim();
});

sw.addEventListener("fetch", (event) => {
  // console.log("[SW] Fetching something...", event);
  // event.respondWith(null); // Custom response
  // event.respondWith(fetch(event.request)); // Keep default response
});
